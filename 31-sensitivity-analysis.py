"""run sensitity analysis with raster maps

(c) 2023, 2024 la. + tb.

input:
+ resulting suitability map as tif
"""

import os
import sys
import subprocess
import numpy as np
import pandas as pd
from osgeo import gdal
import gis_fn as fn
import mcda_config as cfg


def combine_raster(fn1, w1, fn2, w2, outfn):
    """add two raster layers with weights

    there should be a better way to do this without calling
    gdal_calc.py directly, sorry for that.
    """

    subprocess.run(
        [
            "gdal_calc.py",
            "--overwrite",
            "--calc",
            f"(A*{w1})+(B*{w2})",
            "--format",
            "GTiff",
            "--type",
            "Float32",
            "--NoDataValue",
            "-9999",
            "-A",
            fn1,
            "--A_band",
            "1",
            "-B",
            fn2,
            "--B_band",
            "1",
            "--outfile",
            outfn,
            "--co",
            "BIGTIFF=YES",
            "--co",
            "COMPRESS=ZSTD",
            "--co",
            "PREDICTOR=1",
            "--co",
            "ZLEVEL=9",
            "--co",
            "NUM_THREADS=16",
        ],
        shell=False,
        check=True,
    )


def percentage_by_suitabil(raster_file, scenario):
    # Open the raster file
    dataset = gdal.Open(raster_file)
    if dataset is None:
        raise Exception("Failed to open the raster file.")

    # Read the raster data into a numpy array
    raster_array = dataset.ReadAsArray()

    # Define the suitabil value ranges in 0.1 steps up to a maximum of 1.0
    suitabil_ranges = [(i / 10, (i + 1) / 10) for i in range(11)]

    # Create a dictionary to store the percentages for each suitabil range
    percentage_by_suitabil_ranges = {}

    # Iterate through the suitabil value ranges and calculate percentages
    for range_start, range_end in suitabil_ranges:
        # Create a mask for the specified range, including NoData values
        mask = (raster_array >= range_start) & (raster_array < range_end)

        # Calculate the percentage relative to the total number of pixels, excluding NoData
        percentage = (
            np.count_nonzero(mask & (raster_array != -9999))
            / np.count_nonzero(raster_array != -9999)
        ) * 100
        percentage_by_suitabil_ranges[(range_start, range_end)] = percentage

    # Add a first column containing the suitability ranges
    percentage_df = pd.DataFrame.from_dict(
        percentage_by_suitabil_ranges, orient="index", columns=[scenario]
    )
    percentage_df.index = [
        f"{range_start}-{range_end}"
        for range_start, range_end in suitabil_ranges
    ]

    return percentage_df


def suitabil_idx(raster_file):
    # Open the raster file
    dataset = gdal.Open(raster_file)
    if dataset is None:
        raise Exception("Failed to open the raster file.")

    # Read the raster data into a numpy array
    raster_array = dataset.ReadAsArray()

    # Calculate the suitability index as the mean value of non-Nodata cells
    suitabil_index = np.mean(raster_array[raster_array != -9999])

    return suitabil_index


def diff_to_base(raster_file, base_raster_array):
    # Open the raster file
    dataset = gdal.Open(raster_file)
    nodata_value = -9999  # Specify your nodata value
    # Read the raster data into a numpy array
    raster_array = dataset.ReadAsArray()
    raster_array[raster_array == nodata_value] = np.nan

    absolute_changes = np.abs(raster_array - base_raster_array)
    absolute_change = np.nansum(absolute_changes)

    return absolute_change


def weights_matrix_m(weights, vary_weight):
    # Create an empty list to store the resulting weight dictionaries
    weight_matrices = []

    # Include a scenario where the weight of vary_weight is 0
    zero_weight_scenario = {
        key: 0 if key == vary_weight else w / (1 - weights[vary_weight])
        for key, w in weights.items()
    }
    zero_weight_scenario["Scenario"] = f"{vary_weight} =0"
    weight_matrices.append(zero_weight_scenario)

    # Define the range of variations for the specified weight (vary_weight)
    VARIATION_RANGE = cfg.VARIATION_RANGE

    # Iterate through the variations
    for variation in VARIATION_RANGE:
        # Calculate the new weight for the specified weight (vary_weight)
        new_weight_w = weights[vary_weight] * (1 + variation)

        # Create a new dictionary for the weights with the updated value for vary_weight
        new_weights = {
            key: (
                (1 - new_weight_w) * old_weight / (1 - weights[vary_weight])
                if key != vary_weight
                else new_weight_w
            )
            for key, old_weight in weights.items()
        }

        # Include the kind of scenario and the variation in the dictionary
        new_weights["Scenario"] = f"{vary_weight} {variation * 100:.0f}%"

        # Append the new weight dictionary to the list
        weight_matrices.append(new_weights)

    return weight_matrices


if __name__ == "__main__":
    gdal.UseExceptions()
    if len(sys.argv) > 1:
        RESOLUTION = float(sys.argv[1])
    else:
        RESOLUTION = cfg.RESOLUTION

    BASEFN = os.path.join("results", f"result_{int(RESOLUTION)}m.tif")
    OUTFN = os.path.join("sensitivity", f"sens_result_{int(RESOLUTION)}m.tif")
    maps = {
        "uzone": fn.unsaturated_zone_suitability,
        "thickness": fn.aquifer_thickness_suitability,
        "kvalue": fn.hydraulic_cond_suitability,
        "landuse": fn.landuse_suitability,
        "protected": fn.protectedareas_suitability,
        "flooddyn": fn.flooddyn_suitability,
        "elevation": fn.elevation_suitability,
    }

    WEIGHTS = cfg.WEIGHTS

    # read base file
    dataset = gdal.Open(BASEFN)
    nodata_value = -9999  # Specify your nodata value
    # Read the raster data into a numpy array
    base_raster_array = dataset.ReadAsArray()
    base_raster_array[base_raster_array == nodata_value] = np.nan

    # Initialize a list to store the DataFrames for each scenario
    results_data = []

    for m in maps.keys():
        print(f"creating weights matrix for {m}")
        weights_matrix = weights_matrix_m(WEIGHTS, m)

        # Create a sensitivity DataFrame for the current map
        sensitivity_df = pd.DataFrame()
        SENSOUTFN = os.path.join("sensitivity", f"sensitivity_{m}.csv")

        for weights_i in weights_matrix:  ## modify weight of map m
            scenario = weights_i["Scenario"]
            print(f"Weight variation {scenario}")

            i = 0  # Initialize a counter for combining rasters

            for m in maps.keys():  ## overlay maps with new weights
                tif = os.path.join("results", f"{m}_{int(RESOLUTION)}m.tif")
                if i > 0:
                    print(f"combining {m} w/ {OUTFN}")
                    combine_raster(tif, weights_i[m], OUTFN, 1.0, OUTFN)
                else:
                    print(f"initialize {OUTFN} raster w/ {m}")
                    combine_raster(tif, weights_i[m], tif, 0.0, OUTFN)
                i += 1

            print(f"Suitability distribution for weight variation {scenario}")
            # suitabil_distribution = percentage_by_suitabil(OUTFN, scenario)

            # Append the scenario DataFrame to the sensitivity DataFrame
            # sensitivity_df = pd.concat([sensitivity_df, suitabil_distribution], axis=1)

            absolute_change = diff_to_base(OUTFN, base_raster_array)
            # Create a DataFrame for the current scenario and append it to the list
            scenario_df = pd.DataFrame(
                {"Scenario": [scenario], "Absolute Change": [absolute_change]}
            )
            results_data.append(scenario_df)

        # Save the sensitivity DataFrame to an Excel file
        # sensitivity_df.to_csv(SENSOUTFN, index=True)

    # Concatenate all scenario DataFrames into a single DataFrame
    results_df = pd.concat(results_data, ignore_index=True)

    # Write the results to a CSV file
    results_df.to_csv(
        os.path.join("sensitivity", "sensitivity_abs-changes.csv"), index=False
    )
