"""find areas with sufficient unsaturated thickness

(c) 2023 la.

Data requirements:
+ study area
+ drilling logs in study area
+ groundwater information in study area

Results from previous analyses
+ storage area constraint
"""

import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from thiessen_def import generate_thiessen_polygons
from tqdm import tqdm
import gis_fn as fn
import mcda_config as cfg

# read data
studyarea = gpd.read_file(fn.study_area)
drillings = gpd.read_file(fn.drillcoord_study)
groundwater = pd.read_csv(fn.drillgw_study)
dgk_merged = gpd.read_file(fn.storage_capacity_constraint)

THRESHOLD_MIN = cfg.THRESHOLD_MIN
THRESHOLD_MAX = cfg.THRESHOLD_MAX

drillings_gw = drillings.merge(groundwater, on="ObjektID")
drillings_gw["suitabil"] = None

# ignore deep drilllings (> 200 m depth)
drillings_gw = drillings_gw.drop(
    drillings_gw[drillings_gw["Endteufe"] > 200].index
)

for idx, drilling in drillings_gw.iterrows():
    endteufe = float(str(drilling["Endteufe"]))
    if drilling["GWerreicht"] == "Ja":
        rwsp = drilling["RWSP"]
        if (rwsp > THRESHOLD_MIN and rwsp < THRESHOLD_MAX) or pd.isna(rwsp):
            drillings_gw.at[idx, "suitabil"] = 1
        else:
            drillings_gw.at[idx, "suitabil"] = 0
    elif drilling["GWerreicht"] != "Ja":
        if endteufe > THRESHOLD_MIN and endteufe < THRESHOLD_MAX:
            drillings_gw.at[idx, "suitabil"] = 1
        else:
            drillings_gw.at[idx, "suitabil"] = 0

# Deal with more drillings at the same location (keep drilling with
# highest suitability value): Create a dictionary to store unique
# drillings based on rounded geometry and suitability value
unique_drillings_dict = {}


# Define a function to round the coordinates of a Shapely Point
def round_geometry_coordinates(geometry):
    return Point(round(geometry.x, 0), round(geometry.y, 0))


for idx, drilling in drillings_gw.iterrows():
    point_geom = round_geometry_coordinates(drilling["geometry"])
    suitability = drilling["suitabil"]

    if point_geom in unique_drillings_dict:
        # Check if the current drilling has a higher suitability value
        if suitability > unique_drillings_dict[point_geom]["suitabil"]:
            unique_drillings_dict[point_geom] = {
                "geometry": point_geom,
                "suitabil": suitability,
            }
    else:
        unique_drillings_dict[point_geom] = {
            "geometry": point_geom,
            "suitabil": suitability,
        }

# Create a DataFrame from the unique drillings dictionary
drillings_gw = gpd.GeoDataFrame(
    list(unique_drillings_dict.values()), crs=drillings_gw.crs
)

drillings_gw.to_file(fn.unsaturated_zone_thickness)

## constraint map
polygons_gdf = gpd.GeoDataFrame(
    columns=["geometry", "suitabil"], crs=dgk_merged.crs
)
polygons_wo_gdf = gpd.GeoDataFrame()

# Iterate through each geological unit
for index, polygon in tqdm(dgk_merged.iterrows(), total=dgk_merged.shape[0]):
    thiessen_gdf = generate_thiessen_polygons(drillings_gw, polygon, studyarea)

    if not thiessen_gdf.empty:
        polygons_gdf = pd.concat(
            [polygons_gdf, thiessen_gdf], ignore_index=True
        )
        dgk_merged = dgk_merged.drop(index)

    else:  # if no points inside a geological unit
        dgk_merged.loc[index, "suitabil"] = 1
        polygons_wo_gdf = gpd.GeoDataFrame(
            pd.concat(
                [dgk_merged.loc[index:index], polygons_wo_gdf],
                ignore_index=True,
            ),
            geometry="geometry",
        )

polygons_gdf = pd.concat([polygons_gdf, dgk_merged])

# Save the updated polygons shapefile
polygons_gdf.to_file(fn.unsaturated_zone_constraint)
