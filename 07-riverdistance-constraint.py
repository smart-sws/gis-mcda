"""calculate areas within max. distance to river

(c) 2023 la.

Data requirements:
+ shape of study area
+ river network in study area

Results from previous analyses
+ none
"""

import pandas as pd
import geopandas as gpd
from shapely.ops import unary_union
import gis_fn as fn
import mcda_config as cfg

rivers = gpd.read_file(fn.rivers_study)
studyarea = gpd.read_file(fn.study_area)

BUFFER_DISTANCE = cfg.BUFFER_DISTANCE  # m
buffered_rivers = rivers.buffer(BUFFER_DISTANCE)

# Create a GeoDataFrame from the buffers
buffer_gdf = gpd.GeoDataFrame(geometry=buffered_rivers)

# Merge the geometries of all buffers into a single geometry
merged_geometry = unary_union(buffer_gdf.geometry)

# Create a new GeoDataFrame with the merged geometry
merged_gdf = gpd.GeoDataFrame(
    {"geometry_merged": [merged_geometry]}, geometry="geometry_merged"
)
merged_gdf.crs = rivers.crs

# Perform the intersection between the boundary and merged polygons
intersection = gpd.overlay(studyarea, merged_gdf, how="intersection")
intersection["suitabil"] = 1

# Perform the intersection between the boundary and merged polygons
rest = gpd.overlay(studyarea, merged_gdf, how="symmetric_difference")
rest["suitabil"] = 0

river_buffer_gdf = pd.concat([intersection, rest])
river_buffer_gdf = river_buffer_gdf.overlay(
    studyarea, how="intersection", keep_geom_type=True
)  # Clip constraint map to study area


# Print the resulting GeoDataFrame
river_buffer_gdf.to_file(fn.river_distance)
