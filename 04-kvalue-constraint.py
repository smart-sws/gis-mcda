"""find areas which match the contraints for hydraulic conductivity

(c) 2023 la.

Data requirements:
+ geological map of study area

Results from previous analyses
+ areas with sufficient storage capacity in study area
+ definition of aquifers in study area

"""

import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from tqdm import tqdm
from thiessen_def import generate_thiessen_polygons
import gis_fn as fn
import mcda_config as cfg


# read data
studyarea = gpd.read_file(fn.study_area)
drillings = gpd.read_file(fn.drillcoord_study)
layers = pd.read_csv(fn.aquifers)
dgk_merged = gpd.read_file(fn.storage_capacity_constraint)

# read suitable geological units (STRATKL2 for DGK25 Bavaria)
SUITABLE_UNITS = cfg.SUITABLE_UNITS
HCC_THRESHOLD = cfg.HCC_THRESHOLD

# Only keep drilling data where layer information is available
drillings = drillings[drillings["ObjektID"].isin(layers["ObjektID"])]
# Set default suitability of drillings to zero
drillings["suitabil"] = 0

# Iterate through each drilling
for objekt_id, group in tqdm(layers.groupby("ObjektID"), desc="Drillings"):
    for index, layer in group.iterrows():
        layer_hcc = layer[
            "conductivity-class"
        ]  # HCC: hydraulic conductivity class

        if layer_hcc <= HCC_THRESHOLD:  # 1: gravel, 2: loose rock, 3: sand
            drillings.loc[drillings["ObjektID"] == objekt_id, "suitabil"] = 1

# Deal with more drillings at the same location (keep drilling with
# highest suitability value): Create a dictionary to store unique
# drillings based on rounded geometry and suitability value
unique_drillings_dict = {}


# Define a function to round the coordinates of a Shapely Point
def round_geometry_coordinates(geometry):
    return Point(round(geometry.x, 0), round(geometry.y, 0))


for idx, drilling in tqdm(drillings.iterrows(), desc="Suitability"):
    point_geom = round_geometry_coordinates(drilling["geometry"])
    suitability = drilling["suitabil"]

    if point_geom in unique_drillings_dict:
        # Check if the current drilling has a higher suitability value
        if suitability > unique_drillings_dict[point_geom]["suitabil"]:
            unique_drillings_dict[point_geom] = {
                "geometry": point_geom,
                "suitabil": suitability,
            }
    else:
        unique_drillings_dict[point_geom] = {
            "geometry": point_geom,
            "suitabil": suitability,
        }

# Create a DataFrame from the unique drillings dictionary
drillings = gpd.GeoDataFrame(
    list(unique_drillings_dict.values()), crs=drillings.crs
)

drillings.to_file(fn.hydraulic_conductivity)

# constraint map
polygons_gdf = gpd.GeoDataFrame(
    columns=["geometry", "suitabil"], crs=dgk_merged.crs
)

# Iterate through each geological unit
for index, polygon in tqdm(dgk_merged.iterrows(), desc="Geol. units"):
    thiessen_gdf = generate_thiessen_polygons(drillings, polygon, studyarea)

    if not thiessen_gdf.empty:
        polygons_gdf = pd.concat(
            [polygons_gdf, thiessen_gdf], ignore_index=True
        )
        dgk_merged = dgk_merged.drop(index)

    else:  # if no points inside a geological unit
        strati = dgk_merged.loc[index, "STRATKL2"]
        if strati in SUITABLE_UNITS:
            dgk_merged.loc[index, "suitabil"] = 1
        else:
            dgk_merged.loc[index, "suitabil"] = 0

polygons_gdf = pd.concat([polygons_gdf, dgk_merged])
# Save the updated polygons shapefile

polygons_gdf.to_file(fn.hydraulic_cond_constraint)
