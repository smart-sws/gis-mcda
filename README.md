# gis-mcda

python codes and data sources for

L. Augustin & T. Baumann, Suitability Mapping of Subsurface Floodwater Storage Schemes (2024)


2024-07-01: there seems to be a compatibility issue between geopandas (0.14.x),
pandas (>= 2.0) and numpy (>=2.0) preventing successful runs of
16-flooddyn-suitability.py. Things do work with pandas 1.5.x and numpy 1.x

geopandas 1.0.1 has another small issue but is working with recent versions
