#! /bin/sh
echo "MCDA analysis for Smart-SWS"
echo "---------------------------"
echo
PGMDIR=$(dirname "$(readlink -f "$0")")
echo "running analysis for storage constraint criterium"
python $PGMDIR/01-storagearea-constraint.py
echo "running analysis for unsaturated thickness constraint criterium"
python $PGMDIR/02-uzone-constraint.py
echo "running analysis for aquifer constraint criterium"
python $PGMDIR/03-aquifer-definition.py
echo "running analysis for hydraulic conductivity constraint criterium"
python $PGMDIR/04-kvalue-constraint.py
echo "running analysis for landuse constraint criterium"
python $PGMDIR/05-landuse-constraint.py
echo "running analysis for protected areas constraint criterium"
python $PGMDIR/06-protected-contraint.py
echo "running analysis for distance from river constraint criterium"
python $PGMDIR/07-riverdistance-constraint.py
echo "running combination of constraint criteria"
python $PGMDIR/09-constraint-mask.py
echo "running assignment for unsaturated thickness suitability"
python $PGMDIR/11-uzone-suitabil.py
echo "running assignment for aquifer thickness suitability"
python $PGMDIR/12-thickness-suitabil.py
echo "running assignment for hydraulic conductivity suitability"
python $PGMDIR/13-kvalue-suitabil.py
echo "running assignment for landuse suitability"
python $PGMDIR/14-landuse-suitabil.py
echo "running assignment for protected areas suitability"
python $PGMDIR/15-protected-suitabil.py
echo "running analysis and assignment for flood dynamics suitability"
python $PGMDIR/16-flooddyn-suitabil.py
echo "running analysis and assignment hydraulic gradient suitability"
python $PGMDIR/17-elevation-suitabil.py
echo "running combination of suitability maps"
python $PGMDIR/21-suitability-map.py
echo "running sensitivity analysis"
python $PGMDIR/31-sensitivity-analysis.py
