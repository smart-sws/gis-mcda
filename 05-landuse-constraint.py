"""identify areas which are not constrained by land use criteria

(c) 2023 la.

Data requirements:
+ landuse data for study area

Results from previous analyses
+ none
"""

import geopandas as gpd
import gis_fn as fn
import mcda_config as cfg

# read data
landuse = gpd.read_file(fn.landuse_study)
landuse["area"] = landuse.geometry.area

ACCEPTABLE_LANDUSE = cfg.ACCEPTABLE_LANDUSE

landuse["suitabil"] = landuse["nutzart"].apply(
    lambda value: 1
    if value in ACCEPTABLE_LANDUSE
    else 0
)
landuse.to_file(fn.landuse_constraint)
