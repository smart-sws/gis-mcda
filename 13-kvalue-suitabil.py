"""calculate suitability for hydraulic conductivity

(c) 2023 la.


Data requirements:
+ drillings with logs in study area
+ digital geological map for study area

Results from previous analyses
+ map with polygons for storage capacity
+ constraint mask
"""

import geopandas as gpd
import pandas as pd
import numpy as np
from tqdm import tqdm
from thiessen_def import generate_thiessen_polygons
import gis_fn as fn
import mcda_config as cfg
from shapely.geometry import Point

studyarea = gpd.read_file(fn.study_area)
drillings = gpd.read_file(fn.drillcoord_study)
layers = pd.read_csv(fn.aquifers)
dgk_merged = gpd.read_file(fn.storage_capacity_constraint)
constraint_mask = gpd.read_file(fn.constraint_mask)

SUITABLE_UNITS = cfg.SUITABLE_UNITS

# Only keep drilling data where layer information is available
drillings = drillings[drillings["ObjektID"].isin(layers["ObjektID"])]

result_dfs = []  # To store results for each ObjektID

# Iterate through each drilling
for objekt_id, group in layers.groupby("ObjektID"):
    # Initialize dictionaries to store aquifer and hcc thicknesses for each drilling
    aquifer_thickness_dict = {}
    hcc_thickness_dict = (
        {}
    )  # Dictionary to store hcc thicknesses for each aquifer label

    for index, layer in group.iterrows():
        aquifer_label, layer_hcc = (
            layer["aquifer"],
            layer["conductivity-class"],
        )

        if aquifer_label is not np.nan:
            thickness = layer["Untergrenz"] - layer["Obergrenz"]

            if aquifer_label not in aquifer_thickness_dict:
                aquifer_thickness_dict[aquifer_label] = 0
                hcc_thickness_dict[aquifer_label] = {
                    "hcc1": 0,
                    "hcc2": 0,
                    "hcc3": 0,
                }

            aquifer_thickness_dict[aquifer_label] += thickness
            hcc_thickness_dict[aquifer_label][f"hcc{layer_hcc}"] += thickness

    # Calculate suitability values for each aquifer
    suitabil_dict = {}
    for label, hcc_thicknesses in hcc_thickness_dict.items():
        aquifer_thickness = aquifer_thickness_dict[label]
        suitabil = (
            hcc_thicknesses["hcc1"] / aquifer_thickness * 1
            + hcc_thicknesses["hcc2"] / aquifer_thickness * 0.5
            + hcc_thicknesses["hcc3"] / aquifer_thickness * 0.1
        )
        suitabil_dict[label] = suitabil

    # Find the aquifer with the highest suitability value
    best_aquifer = max(suitabil_dict, key=suitabil_dict.get, default=None)

    resulting_suitabil = (
        suitabil_dict.get(best_aquifer, 0) if suitabil_dict else 0
    )
    drillings.loc[drillings["ObjektID"] == objekt_id, "suitabil"] = (
        resulting_suitabil
    )

# Deal with more drillings at the same location (keep drilling with
# highest suitability value): Create a dictionary to store unique
# drillings based on rounded geometry and suitability value
unique_drillings_dict = {}


# Define a function to round the coordinates of a Shapely Point
def round_geometry_coordinates(geometry):
    return Point(round(geometry.x, 0), round(geometry.y, 0))


for idx, drilling in drillings.iterrows():
    point_geom = round_geometry_coordinates(drilling["geometry"])
    suitability = drilling["suitabil"]

    if point_geom in unique_drillings_dict:
        # Check if the current drilling has a higher suitability value
        if suitability > unique_drillings_dict[point_geom]["suitabil"]:
            unique_drillings_dict[point_geom] = {
                "geometry": point_geom,
                "suitabil": suitability,
            }
    else:
        unique_drillings_dict[point_geom] = {
            "geometry": point_geom,
            "suitabil": suitability,
        }

# Create a DataFrame from the unique drillings dictionary
drillings = gpd.GeoDataFrame(
    list(unique_drillings_dict.values()), crs=drillings.crs
)

drillings.to_file(fn.hydraulic_cond_values)

# suitability map
polygons_gdf = gpd.GeoDataFrame(
    columns=["geometry", "suitabil"], crs=dgk_merged.crs
)

# Iterate through each geological unit
for index, polygon in tqdm(dgk_merged.iterrows(), total=dgk_merged.shape[0]):
    thiessen_gdf = generate_thiessen_polygons(drillings, polygon, studyarea)

    if not thiessen_gdf.empty:
        polygons_gdf = pd.concat(
            [polygons_gdf, thiessen_gdf], ignore_index=True
        )
        dgk_merged = dgk_merged.drop(index)

    else:  # if no points inside a geological unit
        strati = dgk_merged.loc[index, "STRATKL2"]

        if strati in SUITABLE_UNITS:
            dgk_merged.loc[index, "suitabil"] = 1
        else:
            dgk_merged.loc[index, "suitabil"] = 0

polygons_gdf = pd.concat([polygons_gdf, dgk_merged])
polygons_suitabil = polygons_gdf.overlay(
    constraint_mask, how="intersection", keep_geom_type=True
)  # clip suitability map to constraint mask

# Save the updated polygons shapefile
polygons_suitabil.to_file(fn.hydraulic_cond_suitability)
