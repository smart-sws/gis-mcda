"""combine contraint criteria for suitability analysis

(c) 2023 la.

Data requirements:
+ extent of study area

Results from previous analyses
+ map with polygons for storage capacity
+ map with polygons for unsaturated zone thickness
+ map with polygons constrained for hydraulic conductivity
+ map with land use constraints
+ map with protected areas
"""

import geopandas as gpd
from shapely.wkt import loads, dumps
import gis_fn as fn

# read results from previous calculations
studyarea = gpd.read_file(fn.study_area)
rivers_distance = gpd.read_file(fn.river_distance)
unsaturated_zone = gpd.read_file(fn.unsaturated_zone_constraint)
land_use = gpd.read_file(fn.landuse_constraint)
protected_areas = gpd.read_file(fn.protectedareas_constraint)
hydraulic_conductivity = gpd.read_file(fn.hydraulic_cond_constraint)
storage_capacity = gpd.read_file(fn.storage_capacity_constraint)

dataframes = [
    rivers_distance,
    unsaturated_zone,
    land_use,
    protected_areas,
    hydraulic_conductivity,
    storage_capacity,
]
filtered_dataframes = []
studyarea_subtracted = studyarea.copy()
studyarea_subtracted = studyarea_subtracted[["geometry"]]
idx = 0


# could we combine the maps with a boolean logic to reduce calculation time?

for dataframe in dataframes:
    idx += 1
    dataframe = dataframe[["suitabil", "geometry"]]
    dataframe["suitabil"] = dataframe["suitabil"].astype("int")

    # what's the reason to restrict the geometry to 3 decimal digits
    # (and not to 0 for a change)
    dataframe.geometry = [
        loads(dumps(geom, rounding_precision=3)) for geom in dataframe.geometry
    ]

    filtered_dataframe = dataframe.loc[dataframe["suitabil"] == 1]
    filtered_dataframe = filtered_dataframe[["geometry"]]
    studyarea_subtracted = gpd.overlay(
        studyarea_subtracted,
        filtered_dataframe,
        how="intersection",
        keep_geom_type=True,
    )

    #    fig3, ax3 = plt.subplots(1)
    #    studyarea.plot(ax=ax3)
    #    filtered_dataframe.plot(ax=ax3, color='green')
    #    plt.show()

    studyarea_subtracted.geometry = [
        loads(dumps(geom, rounding_precision=3))
        for geom in studyarea_subtracted.geometry
    ]

    print(f"substraction of dataframe {str(idx)} done")
    # for debugging uncomment the following line to get access to the
    # last constraint file before error
    # studyarea_subtracted.to_file(fn.constraint_mask)

studyarea_subtracted.reset_index(drop=True, inplace=True)
studyarea_subtracted.to_file(fn.constraint_mask)
