"""identify aquifers in the study area (the lens logic)

(c) 2023 la.

Data requirements:
+ drilling logs in study area
+ groundwater information in study area


Results from previous analyses
+ none
"""

import geopandas as gpd
import pandas as pd
import numpy as np
import gis_fn as fn
import mcda_config as cfg

# min thickness of the permeable layers in m to be counted as aquifer
AQUIFER_THRESHOLD = cfg.AQUIFER_THRESHOLD

# max thickness of lens in m before it is assumed to be hydraulically effective
LENS_THRESHOLD = cfg.LENS_THRESHOLD

# hydraulic conductivity classes
CONDUCTIVITY_CLASSES = cfg.CONDUCTIVITY_CLASSES
HCC_THRESHOLD = cfg.HCC_THRESHOLD

drillings = gpd.read_file(fn.drillcoord_study)
layers = pd.read_csv(fn.drilllayers_study)
layers_sorted = layers.sort_values(["ObjektID", "Obergrenz"])
groundwater = pd.read_csv(fn.drillgw_study)


drillings = drillings[
    drillings["ObjektID"].isin(layers["ObjektID"])
]  # only keep drilling data where layer information is available
drillings = drillings.merge(groundwater, on="ObjektID", how="left")


def get_drilling_info(objekt_id, drillings):
    drilling = drillings[drillings["ObjektID"] == objekt_id]
    if not drilling.empty:
        rwsp = drilling["RWSP"].iloc[0]
        drilling_bottom = drilling["Endteufe"].iloc[0]
        rwsp_conductivity_class = drilling["rwsp-conductivity-class"].iloc[0]
        return rwsp, drilling_bottom, rwsp_conductivity_class
    return None, None, None


# Add a new column to store the conductivity classes
layers_sorted["conductivity-class"] = None
layers_sorted["aquifer"] = None
drillings["rwsp-conductivity-class"] = None

# Iterate through each drilling
for objekt_id, group in layers_sorted.groupby("ObjektID"):
    rwsp = None
    drilling = drillings[drillings["ObjektID"] == objekt_id]
    if not drilling.empty:
        rwsp = drilling["RWSP"].iloc[0]

    # Iterate through each layer of drilling log
    for index, layer in group.iterrows():
        obergrenz, untergrenz, petbez = (
            layer["Obergrenz"],
            layer["Untergrenz"],
            layer["PetBez"],
        )
        layer_conductivity_class = 0

        # Find conductivity class of layer
        for keyword, conductivity_class in CONDUCTIVITY_CLASSES.items():
            if not pd.isna(petbez) and keyword in petbez.lower():
                # Update the maximum conductivity class if a higher
                # one is found -> least conductive one is decisive
                layer_conductivity_class = max(
                    layer_conductivity_class, conductivity_class
                )

        if (
            layer_conductivity_class == 0
        ):  # if no suitable conductivity class found
            layer_conductivity_class = 6

        # Assign the conductivity class to the dataframe
        layers_sorted.at[index, "conductivity-class"] = (
            layer_conductivity_class
        )

        # Get conductivity class of layer containing rwsp
        if rwsp is not None and obergrenz <= rwsp <= untergrenz:
            drillings.loc[
                drillings["ObjektID"] == objekt_id, "rwsp-conductivity-class"
            ] = layer_conductivity_class

idx = -1
# Iterate through each drilling
for objekt_id, group in layers_sorted.groupby("ObjektID"):
    start_aquifer, next_aquifer, lens = False, False, False
    idx += 1
    aquifer_idx, lens_idx = 1, 0

    rwsp, drilling_bottom, rwsp_conductivity_class = get_drilling_info(
        objekt_id, drillings
    )
    aquifer_thickness = 0

    # Iterate through each layer of drilling log
    for index, layer in group.iterrows():
        obergrenz, untergrenz, petbez = (
            layer["Obergrenz"],
            layer["Untergrenz"],
            layer["PetBez"],
        )
        layer_conductivity_class = layer["conductivity-class"]

        if (
            not start_aquifer and layer_conductivity_class <= HCC_THRESHOLD
        ):  # detection of aquifer layer
            start_aquifer = True
            layers_idx = set()  # Initialize layers_idx
            if next_aquifer:
                aquifer_idx += 1

            aquifer_conductivity_class = layer_conductivity_class
            aquifer_thickness = untergrenz - obergrenz
            layers_idx.add(index)

        if (
            start_aquifer
            and layer_conductivity_class <= aquifer_conductivity_class
        ):  # continue with aquifer layer
            lens = False
            aquifer_thickness += untergrenz - obergrenz
            layers_idx.add(index)

        if start_aquifer:
            if (
                not lens
                and layer_conductivity_class > aquifer_conductivity_class
            ):  # detection of lens
                lens = True
                lens_idx += 1
                lens_conductivity_class = layer_conductivity_class
                lens_thickness = untergrenz - obergrenz
                lens_top = obergrenz

            elif lens and (
                layer_conductivity_class >= lens_conductivity_class
                or layer_conductivity_class > HCC_THRESHOLD
            ):  # continue with lens
                lens_thickness += untergrenz - obergrenz

            if (
                lens and lens_thickness > LENS_THRESHOLD
            ):  # lens now assumed to be hydraulically effective
                start_aquifer, next_aquifer, lens = False, False, False

                if aquifer_thickness >= AQUIFER_THRESHOLD:
                    for layer_idx in layers_idx:
                        layers_sorted.at[layer_idx, "aquifer"] = (
                            f"aquifer{aquifer_idx}"
                        )

                if not np.isnan(rwsp) and not rwsp_conductivity_class is None:
                    if (
                        rwsp > lens_top
                        and rwsp_conductivity_class <= HCC_THRESHOLD
                    ):
                        next_aquifer = True
                    else:
                        break  # no other aquifer layer expected
                else:
                    next_aquifer = True
                    # account for possibility of another aquifer layer
                    # if groundwater level not met during drilling

        if untergrenz == drilling_bottom:
            if aquifer_thickness >= AQUIFER_THRESHOLD:
                for layer_idx in layers_idx:
                    layers_sorted.at[layer_idx, "aquifer"] = (
                        f"aquifer{aquifer_idx}"
                    )

layers_sorted.to_csv(fn.aquifers)
