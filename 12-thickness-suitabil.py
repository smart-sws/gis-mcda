"""assign suitability for aquifer thickness

(c) 2023 la.

Data requirements:
+ drilling logs in study area
+ groundwater information in study area

Results from previous analyses
+ constraint mask
+ storage area constraint
+ aquifers in study area
"""

import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from tqdm import tqdm
from thiessen_def import generate_thiessen_polygons
import gis_fn as fn
import mcda_config as cfg

studyarea = gpd.read_file(fn.study_area)
drillings = gpd.read_file(fn.drillcoord_study)
layers = pd.read_csv(fn.aquifers)
dgk_merged = gpd.read_file(fn.storage_capacity_constraint)
constraint_mask = gpd.read_file(fn.constraint_mask)

# Define suitability thresholds for aquifer thickness
THRESHOLD_MIN = cfg.THRESHOLD_MIN  # m
THRESHOLD_MAX = cfg.THRESHOLD_MAX  # m

# Initialize an empty DataFrame to store aquifer information
aquifer_df = pd.DataFrame(columns=["ObjektID", "resulting-thickness"])
# Create a list to store DataFrames for concatenation
dfs_to_concat = []

# Iterate through each drilling
for objekt_id, group in layers.groupby("ObjektID"):
    # Initialize a dictionary to store aquifer thicknesses for each drilling
    aquifer_thickness_dict = {}

    # Iterate through each layer of drilling log
    for index, layer in group.iterrows():
        aquifer_label = layer["aquifer"]

        if aquifer_label is not np.nan:
            thickness = layer["Untergrenz"] - layer["Obergrenz"]

            if aquifer_label not in aquifer_thickness_dict:
                aquifer_thickness_dict[aquifer_label] = 0

            aquifer_thickness_dict[aquifer_label] += thickness

    # Find the resulting thickness below threshold_max and above threshold_min
    thicknesses_within_threshold = [
        thickness
        for thickness in aquifer_thickness_dict.values()
        if THRESHOLD_MIN <= thickness <= THRESHOLD_MAX
    ]

    # Find the maximum of the remaining thicknesses
    max_thickness = max(thicknesses_within_threshold, default=None)

    # Write the result to the aquifer DataFrame
    result_df = pd.DataFrame(
        {"ObjektID": [objekt_id], "resulting-thickness": [max_thickness]}
    )

    # Append the DataFrame to the list
    dfs_to_concat.append(result_df)

# Concatenate all DataFrames in the list
aquifer_df = pd.concat(dfs_to_concat, ignore_index=True)

# suitability assignment
drillings = drillings[
    drillings["ObjektID"].isin(layers["ObjektID"])
]  # only keep drilling data where layer information is available
for idx, row in aquifer_df.iterrows():
    resulting_thickness = row["resulting-thickness"]
    if not np.isnan(resulting_thickness):
        drillings.loc[drillings["ObjektID"] == row["ObjektID"], "suitabil"] = 1
    else:
        drillings.loc[drillings["ObjektID"] == row["ObjektID"], "suitabil"] = 0

# Deal with more drillings at the same location (keep drilling with
# highest suitability value): Create a dictionary to store unique
# drillings based on rounded geometry and suitability value
unique_drillings_dict = {}


# Define a function to round the coordinates of a Shapely Point
def round_geometry_coordinates(geometry):
    return Point(round(geometry.x, 0), round(geometry.y, 0))


for idx, drilling in drillings.iterrows():
    point_geom = round_geometry_coordinates(drilling["geometry"])
    suitability = drilling["suitabil"]

    if point_geom in unique_drillings_dict:
        # Check if the current drilling has a higher suitability value
        if suitability > unique_drillings_dict[point_geom]["suitabil"]:
            unique_drillings_dict[point_geom] = {
                "geometry": point_geom,
                "suitabil": suitability,
            }
    else:
        unique_drillings_dict[point_geom] = {
            "geometry": point_geom,
            "suitabil": suitability,
        }

# Create a DataFrame from the unique drillings dictionary
drillings = gpd.GeoDataFrame(
    list(unique_drillings_dict.values()), crs=drillings.crs
)

drillings.to_file(fn.aquifer_thickness)

# suitability map
polygons_gdf = gpd.GeoDataFrame(
    columns=["geometry", "suitabil"], crs=dgk_merged.crs
)

# Iterate through each geological unit
for index, polygon in tqdm(dgk_merged.iterrows(), total=dgk_merged.shape[0]):
    thiessen_gdf = generate_thiessen_polygons(drillings, polygon, studyarea)

    if not thiessen_gdf.empty:
        polygons_gdf = pd.concat(
            [polygons_gdf, thiessen_gdf], ignore_index=True
        )
        dgk_merged = dgk_merged.drop(index)

    else:  # if no points inside a geological unit
        dgk_merged.loc[index, "suitabil"] = 0.5

polygons_gdf = pd.concat([polygons_gdf, dgk_merged])
polygons_suitabil = polygons_gdf.overlay(
    constraint_mask, how="intersection", keep_geom_type=True
)  # clip suitability map to constraint mask

# Save the updated polygons shapefile
polygons_suitabil.to_file(fn.aquifer_thickness_suitability)
