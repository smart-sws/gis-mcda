"""Collect data from open sources (if not yet present on disk)
and clip to given studyarea

(c) tb. 2023, 2024

studyarea could be
+ an administrative district
+ a river catchment (coming soon)
+ a defined shape (coming soon)

we assume a folder structure

current working directory w/ python scripts
  |
  |_ admin           : administrative boundaries
  |_ dem             : digital elevation models
     |_ tiles        : temporary folder for downloaded DEM tiles
  |_ drillings       : drilling logs and base data
  |_ protection      : protection zones
  |_ landuse         : landuse data
  |_ gauges          : information about gauges
  |_ rivernet        : copy of EU-Hydro for the catchment of the rivers
  |_ geology         : geological maps
  |_ results         : results of the simulation steps

"""

import os
import sys
import re
import gc
import zipfile
from fnmatch import fnmatch
import subprocess
from math import floor
import numpy as np
import pandas as pd
import geopandas as gpd
import requests
from osgeo import gdal
import shapely as shp
from tqdm import tqdm
import gis_fn as fn

gc.enable()
gdal.UseExceptions()

# this is the prefix for the source data before any clipping
# should be somewhere outside the working directory where downloaded
# files can be stored permanently eg. ~/gis/suitability_srcdata
SRCPREFIX = os.path.join(os.path.expanduser("~"), "gis", "suitability_srcdata")
# SRCPREFIX = os.path.join("/", "data", "gis", "suitability_srcdata")
# this is the prefix for all local data for the analysis
# we assume that prepare_gisdata is launched from the working
# directory of the analysis
PREFIX = os.path.join(os.getcwd())

PROJECT_CRS = "EPSG:25832"

LFUSERVER = "https://www.lfu.bayern.de/gdi/dls/daten"
SOURCE_URLS = {
    # copernicus river data needs registration and produces river network on demand
    # "https://land.copernicus.eu/en/products/eu-hydro/eu-hydro-river-network-database#Download",
    "rivernet": "",
    "admin": "https://gisco-services.ec.europa.eu/"
    + "distribution/v2/nuts/download/"
    + "ref-nuts-2021-01m.shp.zip",
    # which landuse data are we using ATKIS or ATKIS-TN
    "landuse": "https://geodaten.bayern.de/odd/m/2/basisdlm/bkg_shape/bkg_shape.zip",
    "landuse_actual": "https://geodaten.bayern.de/odd/m/3/daten/tn/Nutzung_kreis.gpkg",
    "geology": f"{LFUSERVER}/dgk25/dgk25_epsg25832_shp.zip",
    "dem": "https://download1.bayernwolke.de/a/dgm/dgm1",
    "dem5": "https://download1.bayernwolke.de/a/dgm/dgm5xyz",
    "drillings": f"{LFUSERVER}/bohrungen/bohrungen_epsg25832_shp.zip",
    "drinkingwater": f"{LFUSERVER}/wsg/twsg_epsg25832_shp.zip",
    "spawater": f"{LFUSERVER}/wsg/hqsg_epsg25832_shp.zip",
    "ffh-areas": f"{LFUSERVER}/natura2000/ffh_epsg25832_shp.zip",
    "bird-prot": f"{LFUSERVER}/natura2000/vogelschutz_epsg25832_shp.zip",
    "nationalparcs": f"{LFUSERVER}/schutzgebiete/nlp_epsg25832_shp.zip",
    "naturalpreserve": f"{LFUSERVER}/schutzgebiete/nsg_epsg25832_shp.zip",
    "landscapeprot": f"{LFUSERVER}/schutzgebiete/lsg_epsg25832_shp.zip",
    "natureparcs": f"{LFUSERVER}/schutzgebiete/naturparke_epsg25832_shp.zip",
    "gauges": f"{LFUSERVER}/pegel/pegel_epsg25832_shp.zip",
    # gauge information are currently handled in the subroutines
    "gkd": "",
    "hnd": "",
    "gauges_combined": "",
}

# all source filenames are given as lists to ease the transfer from linux to win.
SRC_FNS = {
    "rivers": [SRCPREFIX, "rivernet", "EU-Hydro_gdb.gdb"],
    "admin": [SRCPREFIX, "admin", "NUTS_RG_01M_2021_3035.shp"],
    "landuse": [SRCPREFIX, "landuse", "atkis_combined.gpkg"],
    "landuse_actual": [SRCPREFIX, "landuse", "Nutzung_kreis.gpkg"],
    "geology": [SRCPREFIX, "geology", "GeolEinheit.shp"],
    "dem": [SRCPREFIX, "dem", ""],
    "dem5": [SRCPREFIX, "dem", ""],
    "drill_shp": [SRCPREFIX, "drillings", "bohrungen.shp"],
    "drill_basedata": [SRCPREFIX, "drillings", "bohrungen_stammdaten.csv"],
    "drill_groundwater": [SRCPREFIX, "drillings", "bohrungen_grundwasser.csv"],
    "drill_layers": [SRCPREFIX, "drillings", "bohrungen_schichten.csv"],
    "drill_sublayers": [SRCPREFIX, "drillings", "bohrungen_teilschichten.csv"],
    "drinkingwater": [SRCPREFIX, "protection", "twsg_epsg25832.shp"],
    "spawater": [SRCPREFIX, "protection", "hqsg_epsg25832.shp"],
    "ffh-areas": [SRCPREFIX, "protection", "ffh_epsg25832_shp.shp"],
    "bird-prot": [SRCPREFIX, "protection", "vogelschutz_epsg25832_shp.shp"],
    "nationalparcs": [SRCPREFIX, "protection", "nlp_epsg25832_shp.shp"],
    "naturalpreserve": [SRCPREFIX, "protection", "nsg_epsg25832_shp.shp"],
    "landscapeprot": [SRCPREFIX, "protection", "lsg_epsg25832_shp.shp"],
    "natureparcs": [SRCPREFIX, "protection", "naturparke_epsg25832_shp.shp"],
    "gauges": [SRCPREFIX, "gauges", "pegel_epsg25832.shp"],
    "gkd": "",
    "hnd": "",
    "gauges_combined": [SRCPREFIX, "gauges", "by-gkd-hnd.csv"],
}


def prepare_directories(toplevel):
    for d in [
        "admin",
        os.path.join("dem", "tiles"),
        "drillings",
        "protection",
        "landuse",
        "gauges",
        "rivernet",
        "geology",
        "results",
        "sensitivity",
    ]:
        directory = os.path.join(toplevel, d)
        if not os.path.exists(directory):
            os.makedirs(directory)


# helper functions
def getval(datastr):
    result = re.match(r"^[0-9.,]+", datastr)
    if result:
        result = re.sub(r"\.", "", result[0])
        result = re.sub(r",", ".", result)
    else:
        result = -9e99
    return float(result)


def get_lfudata_generic(what, studyarea, buf=0, force_download=False):
    """generic function to retrieve shapefiles
    from the servers of the Bavarian environmental agency
    extract and store in directory
    """

    fn = os.path.join(*SRC_FNS[what])
    directory = os.path.join(*SRC_FNS[what][:-1])
    os.makedirs(directory, exist_ok=True)
    if force_download or not os.path.exists(fn):
        if SOURCE_URLS[what] == "":
            print(f"Automated download of {what} is not possible")
            return
        zip_fn = re.sub(".shp", "_shp", SRC_FNS[what][-1]) + ".zip"
        download = requests.get(SOURCE_URLS[what])
        download_fn = os.path.join(directory, zip_fn)
        with open(download_fn, mode="wb") as localfile:
            localfile.write(download.content)

        with zipfile.ZipFile(download_fn) as zfile:
            for zip_info in zfile.infolist():
                if zip_info.is_dir():
                    continue
                zip_info.filename = os.path.basename(zip_info.filename)
                zfile.extract(zip_info, directory)

    dataset = gpd.read_file(fn).to_crs(PROJECT_CRS)

    if studyarea is not None:
        if buf > 0:
            dataset["geometry"] = dataset.geometry.buffer(buf)
        dataset = dataset.clip(studyarea)
        # dataset = dataset.overlay(studyarea, how="intersection")
        # la. uses gpd.clip(dataset, studyarea) we should benchmark
        # the two options => no significant difference w/ atkis but
        # overlay adds the information from the admin file (which
        # doesn't harm)

        # actually it seems that some of the information
        # causes problems with saving the files

    return dataset.to_crs(PROJECT_CRS)


def get_admin(force_download=False):
    """get administrative data from EU statistics or another source,
    retrieve data from online source if not yet present or if
    force_download=True

    store fileset in ./admin

    We use EU data for maximum flexibility
    (and because county level is more than enough)

    Useful fields are NAME_LATN, NUTS_ID

    NUTS_ID for Swabia is DE27
    """

    fn = os.path.join(*SRC_FNS["admin"])
    directory = os.path.join(*SRC_FNS["admin"][:-1])
    os.makedirs(directory, exist_ok=True)
    if force_download or not os.path.exists(fn):
        zip_fn = "nuts_2021.zip"
        download_fn = os.path.join(directory, zip_fn)
        download = requests.get(SOURCE_URLS["admin"])
        with open(download_fn, mode="wb") as localfile:
            localfile.write(download.content)
        with zipfile.ZipFile(download_fn, "r") as zfile:
            zfile.extract("NUTS_RG_01M_2021_3035.shp.zip", directory)
        with zipfile.ZipFile(
            os.path.join(directory, "NUTS_RG_01M_2021_3035.shp.zip"), "r"
        ) as zfile:
            zfile.extractall(directory)

    dataset = gpd.read_file(fn).to_crs(PROJECT_CRS)
    return dataset


def get_drillings(force_download=False):
    """get drilling data from Bavarian Environmental Agency

    this function will always fetch the full data set and is called by
    drill_coordinates, drill_layers, drill_sublayers, drill_basedata
    """

    fn = os.path.join(*SRC_FNS["drill_shp"])
    directory = os.path.join(*SRC_FNS["drill_shp"][:-1])
    os.makedirs(directory, exist_ok=True)
    if force_download or not os.path.exists(fn):
        if SOURCE_URLS["drillings"] == "":
            print("Automated download of drillings data is not possible")
            return
        download = requests.get(SOURCE_URLS["drillings"], timeout=90)
        download_fn = os.path.join(directory, "bohrungen_epsg25832_shp.zip")
        with open(download_fn, mode="wb") as localfile:
            localfile.write(download.content)
        with zipfile.ZipFile(download_fn, "r") as zfile:
            zfile.extractall(directory)


def read_drill_coordinates(studyarea=None, buf=0):
    """get shapefile with all drillings

    in studyarea if not None
    and with buffer buf around studyarea

    studyarea is a gpd.DataFrame
    """

    fn = os.path.join(*SRC_FNS["drill_shp"])
    if not os.path.exists(fn):
        get_drillings()
    dataset = gpd.read_file(fn).to_crs(PROJECT_CRS)
    if studyarea is not None:
        if buf > 0:
            dataset["geometry"] = dataset.geometry.buffer(buf)
        dataset = dataset.clip(studyarea)

    return dataset.to_crs(PROJECT_CRS)


def read_drill_layers(studyarea=None, buf=0):
    """get dataset for all layers in drilling logs"""

    fn = os.path.join(*SRC_FNS["drill_layers"])
    if not os.path.exists(fn):
        get_drillings()
    dataset = pd.read_csv(fn, sep=";", decimal=",", na_values="NaN")

    if studyarea is not None:
        subset = read_drill_coordinates(studyarea, buf)
        dataset = dataset[dataset["ObjektID"].isin(subset["ObjektID"])]

    return dataset


def read_drill_basedata(studyarea=None, buf=0):
    """get dataset for basedata in drilling logs"""

    fn = os.path.join(*SRC_FNS["drill_basedata"])
    if not os.path.exists(fn):
        get_drillings()

    dataset = pd.read_csv(fn, sep=";", decimal=",", na_values="NaN")

    if studyarea is not None:
        subset = read_drill_coordinates(studyarea, buf)
        dataset = dataset[dataset["ObjektID"].isin(subset["ObjektID"])]

    return dataset


def read_drill_groundwater(studyarea=None, buf=0):
    """get dataset for all groundwater in drilling logs"""

    fn = os.path.join(*SRC_FNS["drill_groundwater"])
    if not os.path.exists(fn):
        get_drillings()

    dataset = pd.read_csv(fn, sep=";", decimal=",", na_values="NaN")

    if studyarea is not None:
        subset = read_drill_coordinates(studyarea, buf)
        dataset = dataset[dataset["ObjektID"].isin(subset["ObjektID"])]

    return dataset


def get_dGK25(force_download=False):
    """download dGK data"""

    fn = os.path.join(*SRC_FNS["geology"])
    directory = os.path.join(*SRC_FNS["geology"][:-1])
    os.makedirs(directory, exist_ok=True)
    if force_download or not os.path.exists(fn):
        if SOURCE_URLS["geology"] == "":
            print("Automated download of geology data is not possible")
            return
        download = requests.get(SOURCE_URLS["geology"])
        download_fn = os.path.join(directory, "dgk25_epsg25832_shp.zip")

        with open(download_fn, mode="wb") as localfile:
            localfile.write(download.content)

        with zipfile.ZipFile(download_fn) as zfile:
            for zip_info in zfile.infolist():
                if zip_info.is_dir():
                    continue
                zip_info.filename = os.path.basename(zip_info.filename)
                zfile.extract(zip_info, directory)


def read_geology(studyarea=None, buf=0, force_download=False):
    """get geology data for studyarea"""

    fn = os.path.join(*SRC_FNS["geology"])
    if not os.path.exists(fn):
        get_dGK25(force_download)

    dataset = gpd.read_file(fn).to_crs(PROJECT_CRS)

    if studyarea is not None:
        if buf > 0:
            dataset["geometry"] = dataset.geometry.buffer(buf)
        dataset = dataset.clip(studyarea)

    return dataset.to_crs(PROJECT_CRS)


def read_rivernet(studyarea=None, buf=0, force_download=False):
    """read EU-Hydro river network for study area

    please note that EU-Hydro requires a registration since late 2023
    data is still open access but cannot be downloaded automatically

    files are expected in ./rivers/EU-Hydro_gdb.gdb/
    and are read as a FileDB
    """

    fn = os.path.join(*SRC_FNS["rivers"])
    dataset = gpd.read_file(fn, driver="FileGDB", layer="River_Net_l").to_crs(
        PROJECT_CRS
    )
    dataset = dataset.explode(ignore_index=True)

    if studyarea is not None:
        if buf > 0:
            dataset["geometry"] = dataset.geometry.buffer(buf)
        dataset = dataset.clip(studyarea)

    return dataset.to_crs(PROJECT_CRS)


def read_landuse(studyarea=None, buf=0, force_download=False):
    """read landuse data for study area

    this is the base dataset (which was not used in this study)

    please note that an update for the file format GeoInfoDok 6.01 -> 7.1.2
    was announced for 2024-01-01

    """

    ### Note to self: Do we have to read the full dataset or is it possible
    ### to query the features within the studyarea

    fn = os.path.join(*SRC_FNS["landuse"])
    directory = os.path.join(*SRC_FNS["landuse"][:-1])
    os.makedirs(directory, exist_ok=True)
    if force_download or not os.path.exists(fn):
        if SOURCE_URLS["landuse"] == "":
            print("Automated download of landuse data is not possible")
            return
        download = requests.get(SOURCE_URLS["landuse"])
        download_fn = os.path.join(directory, "bkg_shape.zip")

        with open(download_fn, mode="wb") as localfile:
            localfile.write(download.content)

        with zipfile.ZipFile(download_fn) as zfile:
            for zip_info in zfile.infolist():
                if zip_info.is_dir():
                    continue
                zip_info.filename = os.path.basename(zip_info.filename)
                zfile.extract(zip_info, directory)

        # code to combine files copied from la. (pretty smart python code ;-)
        file_list = []
        # we use only polygon data "*_f" (no mixing of geometries in shapefiles)
        pattern = "*_f.shp"
        for path, subdirs, files in os.walk(directory):
            for name in files:
                if fnmatch(name, pattern):
                    file_list.append(os.path.join(path, name))

        landuse = pd.concat(
            [gpd.read_file(shp) for shp in file_list],
            axis=0,
            ignore_index=True,
        ).pipe(gpd.GeoDataFrame)
        landuse.to_file(fn)

    # reading and writing the full dataset is really slow
    # we should consider to save files for the studyarea
    # as we do for dgm1
    # or use ogr which seems to much faster

    dataset = gpd.read_file(fn).to_crs(PROJECT_CRS)

    if studyarea is not None:
        if buf > 0:
            dataset["geometry"] = dataset.geometry.buffer(buf)
        dataset = dataset.clip(studyarea)

    return dataset


def read_landuse_actual(studyarea, admin, buf=0, force_download=False):
    """read actual landuse data for study area

    ATKIS-TN contains information on a county level, that should simplify
    and speed up operations significantly
    """

    fn = os.path.join(*SRC_FNS["landuse_actual"])
    directory = os.path.join(*SRC_FNS["landuse_actual"][:-1])
    os.makedirs(directory, exist_ok=True)
    if force_download or not os.path.exists(fn):
        if SOURCE_URLS["landuse_actual"] == "":
            print("Automated download of landuse data is not possible")
            return
        download = requests.get(SOURCE_URLS["landuse_actual"])
        # ATKIS-TN is provided as a GeoPackage,
        # no need to unzip or to combine layers.

        with open(fn, mode="wb") as localfile:
            localfile.write(download.content)

    # reading and writing the full dataset is really slow
    # we should consider to save files for the studyarea
    # as we do for dgm1
    # or use ogr which seems to much faster

    # dataset = gpd.read_file(fn)

    # How do we get the counties inside or touched a river
    # catchment or an arbitrary shapefile?

    # TODO: Logic for selecting the studyarea has to be adapted
    if studyarea is not None:
        nuts_id = studyarea["NUTS_ID"].values[0]
        # if studyarea["LEVL_CODE"].values[0] < 3:
        while len(nuts_id) < 5:  # expand to NUTS3,  we want districs only
            nuts_id += "."
        layers = []
        counties = admin.query(f"NUTS_ID.str.contains('{nuts_id}')")[
            "NAME_LATN"
        ]
        for county in counties:
            mycounty = county.split(",")
            # EU data has a space after the abbreviations, Bavarian data not.
            mycounty[0] = re.sub(r"\.[ ]*", ".", mycounty[0])
            # unfortunately the data is not consistent between NUTS und ALKIS TN
            # we have to adapt for many different cases
            mycounty[0] = re.sub(r"Opf$", "OPf.", mycounty[0])
            if len(mycounty) > 1:
                if re.search(r"Stadt", mycounty[1].strip()):
                    layers.append(f"Stadt {mycounty[0]}")
                else:
                    layers.append(f"Landkreis {mycounty[0]}")
            else:
                layers.append(f"Landkreis {mycounty[0]}")

            dataset = pd.concat(
                [gpd.read_file(fn, layer=lay) for lay in layers],
                axis=0,
                ignore_index=True,
            ).pipe(gpd.GeoDataFrame)

    return dataset


def get_dem1(studyarea, buf, force_download=False):
    """Fetch tiles from opendata Bayern for studyarea
    and convert into one geotiff.

    this code uses the following linux commands

    gdal_merge.py

    not sure whether this works on other architectures
    """

    URL = SOURCE_URLS["dem"]
    # get NUTS_ID for studyarea
    directory = os.path.join(*SRC_FNS["dem"][:-1])
    dem1fn = fn.dem1_study_tif

    if force_download or not os.path.exists(dem1fn):
        tiles_dir = os.path.join(*SRC_FNS["dem"][:-1], "tiles")
        studytiles = os.path.join(
            *SRC_FNS["dem"][:-1],
            "tiles",
        )

        os.makedirs(directory, exist_ok=True)
        os.makedirs(tiles_dir, exist_ok=True)
        os.makedirs(studytiles, exist_ok=True)
        coords = studyarea.geometry.buffer(buf).envelope.get_coordinates()

        xl = int(min(coords["x"]) // 1000)
        xr = int(max(coords["x"]) // 1000)
        yb = int(min(coords["y"]) // 1000)
        yt = int(max(coords["y"]) // 1000)

        # fetch tiles
        # TODO: check if tiles are available locally
        #       if so symlink into studytiles

        for x in tqdm(
            range(xl, xr + 1), position=0, desc="x", leave=False, ncols=70
        ):
            for y in tqdm(
                range(yb, yt + 1), position=1, desc="y", leave=False, ncols=70
            ):
                # test if tile is in polygon
                p_wkt = [
                    f"POLYGON (({x*1000} {y*1000},"
                    + f"{(x+1)*1000} {y*1000},"
                    + f"{(x+1)*1000} {(y+1)*1000},"
                    + f"{x*1000} {(y+1)*1000},"
                    + f"{x*1000} {y*1000} ))"
                ]
                p = gpd.GeoDataFrame(
                    geometry=gpd.GeoSeries.from_wkt(p_wkt, crs="EPSG:25832")
                )
                if len(gpd.sjoin(p, studyarea, predicate="intersects")) > 0:
                    tilename = f"{x}_{y}.tif"
                    if not os.path.exists(
                        os.path.join(studytiles, f"{tilename}")
                    ):
                        download = requests.get(f"{URL}/{tilename}")
                        if download.ok:
                            with open(
                                os.path.join(studytiles, f"{tilename}"), "wb"
                            ) as f:
                                f.write(download.content)
                    # proc = subprocess.Popen(
                    #     [
                    #         "curl",
                    #         "-s",
                    #         f"{URL}/{tilename}",
                    #         "-o",
                    #         os.path.join(tmpdir, f"{tilename}"),
                    #     ],
                    #     stdout=subprocess.DEVNULL,
                    # )
                    # proc.wait()

        file_list = []
        pattern = "*.tif"
        for path, subdirs, files in os.walk(studytiles):
            for name in files:
                if fnmatch(name, pattern):
                    file_list.append(os.path.join(path, name))

        proc = subprocess.Popen(
            # we have to use shell=True syntax here to allow globs
            f'gdal_merge.py -co "BIGTIFF=YES" -co "COMPRESS=LZW" -co "PREDICTOR=2"'
            + f' -co "TILED=YES" -o {fn.dem1_study_tif} {os.path.join(studytiles, "*.tif")}',
            shell=True,
        )
        proc.wait()


def get_dem5(studyarea, buf, force_download=False):
    """Fetch tiles from opendata Bayern for studyarea
    and convert into one geodataframe.

    this code uses the following linux commands

    gdal_merge.py

    not sure whether this works on other architectures
    """

    URL = SOURCE_URLS["dem5"]
    directory = os.path.join(*SRC_FNS["dem5"][:-1])
    dem5fn = fn.dem5_study_tif

    if force_download or not os.path.exists(dem5fn):
        tiles_dir = os.path.join(*SRC_FNS["dem5"][:-1], "tiles")
        studytiles = os.path.join("dem", "tiles")

        os.makedirs(directory, exist_ok=True)
        os.makedirs(tiles_dir, exist_ok=True)
        os.makedirs(studytiles, exist_ok=True)
        coords = studyarea.geometry.buffer(buf).envelope.get_coordinates()

        xl = int(min(coords["x"]) // 1000)
        xr = int(max(coords["x"]) // 1000)
        yb = int(min(coords["y"]) // 1000)
        yt = int(max(coords["y"]) // 1000)

        # fetch tiles
        # TODO: check if tiles are available locally
        #       if so symlink into studytiles

        file_list = []
        for x in tqdm(range(xl, xr + 1)):
            for y in range(yb, yt + 1):
                # test if tile is in polygon
                p_wkt = [
                    f"POLYGON (({x*1000} {y*1000},"
                    + f"{(x+1)*1000} {y*1000},"
                    + f"{(x+1)*1000} {(y+1)*1000},"
                    + f"{x*1000} {(y+1)*1000},"
                    + f"{x*1000} {y*1000} ))"
                ]
                p = gpd.GeoDataFrame(
                    geometry=gpd.GeoSeries.from_wkt(p_wkt, crs="EPSG:25832")
                )
                if len(gpd.sjoin(p, studyarea, predicate="intersects")) > 0:
                    tilename = f"{x}_{y}"
                    # save individual tiles for the sake of better performance
                    tfn = os.path.join(studytiles, f"{tilename}.txt")
                    if os.path.exists(tfn):
                        # tile is present, append to list
                        file_list.append(tfn)
                    else:
                        download = requests.get(f"{URL}/{tilename}.zip")
                        if download.ok:
                            with open(f"{tilename}.zip", "wb") as f:
                                f.write(download.content)
                            with zipfile.ZipFile(
                                f"{tilename}.zip", "r"
                            ) as zfile:
                                zfile.extractall(studytiles)
                            # tile was successfully downloaded, append to list
                            file_list.append(tfn)

        # combine tiles to raster
        # TODO: implement a logic to merge only subsets of the data.
        #       or clip to studyarea
        #       This will be useful, if we work on eg. a county DE27C which
        #       is in a region we already calculated
        #       would anyone like to do that, probably not
        #       because we can zoom into the suitability?

        proc = subprocess.Popen(
            # we have to use shell=True syntax here to allow globs
            f'gdal_merge.py -co "BIGTIFF=YES" -co "COMPRESS=LZW" -co "PREDICTOR=1" '
            + '-co "ZLEVEL=9" -co "NUM_THREADS=16" '
            + f'-co "TILED=YES" -o {fn.dem5_study_tif} '
            + f'{os.path.join(studytiles,"*.txt")}',
            shell=True,
        )
        proc.wait()


def drape_rivernet(resolution=25):
    """drape the river network on the dem (get z values)

    Note: although it seems to be an obvious choice to use the Z data from
    the river segments in EU Hydro, these are sometimes off by more than
    20 m in narrow valleys. The reason for this is a coarse
    DEM. Therefore, we need to drape the rivers onto the DEM manually.
    """

    rivers = gpd.read_file(fn.rivers_study)

    dem = gdal.Open(fn.dem5_study_tif)
    gt_forward = dem.GetGeoTransform()
    gt_reverse = gdal.InvGeoTransform(gt_forward)
    elevation = dem.GetRasterBand(1)

    for idx, line in rivers.iterrows():
        splitted = shp.segmentize(line.geometry, 25)
        geom = None
        # working around a bug in shapely/geos which does not interpolate Z
        for xyz in shp.get_coordinates(splitted, include_z=True):
            x = xyz[0]
            y = xyz[1]
            z = xyz[2]
            px, py = gdal.ApplyGeoTransform(gt_reverse, x, y)
            newz = float(
                elevation.ReadAsArray(floor(px), floor(py), 1, 1)[0]
            )  # validated manually
            xyz[2] = newz
            if geom is None:
                geom = np.array(xyz)
            else:
                geom = np.vstack([geom, xyz])
        newgeometry = shp.set_coordinates(splitted, geom)
        rivers.loc[idx, "geometry"] = newgeometry
        rivers = rivers.explode(ignore_index=True)

        return rivers


# Gauges
def read_gauge_shape(studyarea=None, buf=0, force_download=False):
    """read gauges (in study_area)"""
    return get_lfudata_generic("gauges", studyarea, buf, force_download)


def get_gkd_gauges(parameter="discharge"):
    url = f"https://www.gkd.bayern.de/en/rivers/{parameter}/tables"
    dfs = pd.read_html(
        url, decimal=",", extract_links="body", displayed_only=False
    )
    return dfs[0]


def get_hnd_gauges():
    url = f"https://www.hnd.bayern.de/pegel/meldestufen/tabellen"
    dfs = pd.read_html(
        url, decimal=",", extract_links="body", displayed_only=False
    )
    return dfs[0]


def get_gauge_basedata(region, gauge):
    baseurl = "https://www.hnd.bayern.de/pegel"
    url = f"{baseurl}/{region}/{gauge}/stammdaten?"
    dfs = pd.read_html(url)
    return dfs[0]


def get_hnd_data(region, gauge):
    baseurl = "https://www.hnd.bayern.de/pegel"
    url = f"{baseurl}/{region}/{gauge}/statistik?"
    dfs = pd.read_html(url)
    return dfs[-1]


def get_hnd_upstream(region, gauge):
    baseurl = "https://www.hnd.bayern.de/pegel"
    url = f"{baseurl}/{region}/{gauge}/gebiet?"
    hnd = requests.get(url)

    region = []
    pegel = []
    unique_gauges = set()  # Use a set to store unique gauge values
    if hnd.ok:
        raw_upstream = re.split("Oberhalb", str(hnd.content))

        if len(raw_upstream) > 1:
            upstream_iter = re.finditer(
                r"https://www.hnd.bayern.de/pegel/(.+?)/(.+?)/gebiet",
                raw_upstream[1],
                re.MULTILINE,
            )

            for upstream in upstream_iter:
                current_gauge = upstream.group(2)
                if current_gauge not in unique_gauges:
                    unique_gauges.add(current_gauge)
                    region += [
                        upstream.group(1),
                    ]
                    pegel += [
                        current_gauge,
                    ]
    return pegel, region


# wrapper functions for protection zones and other data from Bavarian LfU
def read_drinkingwater_protection(studyarea=None, buf=0, force_download=False):
    """read drinkingwater protection zones for studyarea"""

    return get_lfudata_generic("drinkingwater", studyarea, buf, force_download)


def read_spawater_protection(studyarea=None, buf=0, force_download=False):
    """read spawater protection zones for studyarea"""

    return get_lfudata_generic("spawater", studyarea, buf, force_download)


def read_ffh_areas(studyarea=None, buf=0, force_download=False):
    """read FFH protection zones (Natura2000) for studyarea"""

    return get_lfudata_generic("ffh-areas", studyarea, buf, force_download)


def read_bird_protection(studyarea=None, buf=0, force_download=False):
    """read bird protection zones (Natura2000) for studyarea"""

    return get_lfudata_generic("bird-prot", studyarea, buf, force_download)


def read_nationalparcs(studyarea=None, buf=0, force_download=False):
    """read national parc areas for studyarea"""

    return get_lfudata_generic("nationalparcs", studyarea, buf, force_download)


def read_natural_preserves(studyarea=None, buf=0, force_download=False):
    """read national preserves for studyarea"""

    return get_lfudata_generic(
        "naturalpreserve", studyarea, buf, force_download
    )


def read_landscape_protection(studyarea=None, buf=0, force_download=False):
    """read landscape protection zones for studyarea"""

    return get_lfudata_generic("landscapeprot", studyarea, buf, force_download)


def read_natureparcs(studyarea=None, buf=0, force_download=False):
    """read nature parcs for studyarea"""

    return get_lfudata_generic("natureparcs", studyarea, buf, force_download)


# wrapper for combined gauge data
def read_combined_gauges(force_download=False):
    """Collect/scrape gauge data from different websites and combine
    into a csv-file"""

    # check if data is available locally
    fn = os.path.join(*SRC_FNS["gauges_combined"])
    directory = os.path.join(*SRC_FNS["gauges_combined"][:-1])
    if os.path.exists(fn) and not force_download:
        result = pd.read_csv(fn)
        return result

    # if not start scraping the data
    df = get_gkd_gauges(parameter="discharge")

    gkd = pd.DataFrame()
    for idx, mainrow in df.iterrows():
        data = pd.DataFrame()
        url = mainrow[0][1] + "/statistics"
        info = pd.read_html(url)

        gauge_id = info[0].iloc[0, 1]

        try:
            discharge = info[1]
            for idx, row in discharge.iterrows():
                if row[0] in ("NQ", "MNQ", "MQ", "MHQ", "HQ"):
                    data.at[gauge_id, row[0]] = float(row.iloc[3])

            data.at[gauge_id, "water_body"] = mainrow[1][0]

            gkd = pd.concat((gkd, data))
        except:
            ...
        idx += 1

    # get hnd data
    hnd = pd.DataFrame()
    gauges = get_hnd_gauges()

    i = 1
    for idx, row in gauges.iterrows():
        data = pd.DataFrame()
        site_re = re.match(
            r"https://www.hnd.bayern.de/pegel/([a-z_]+)/([a-z-]+)-([0-9]+).*",
            row[0][1],
        )
        if site_re:
            region = site_re[1]
            gauge = site_re[2]
            gauge_id = site_re[3]

            data.at[gauge_id, "region"] = region
            data.at[gauge_id, "gauge"] = gauge

            upstream_pegel, upstream_region = get_hnd_upstream(
                region, f"{gauge}-{gauge_id}"
            )
            for j in range(len(upstream_region)):
                data.at[gauge_id, f"up_id{j}"] = upstream_pegel[j]
                data.at[gauge_id, f"up_region{j}"] = upstream_region[j]

            try:
                gauge_bd = get_gauge_basedata(region, gauge + "-" + gauge_id)
            except:
                continue

            for idx, row in gauge_bd.iterrows():
                if row[0] == "Einzugsgebiet:":
                    data.at[gauge_id, "catch_area"] = getval(row[1])
                if row[0] == "Flußkilometer:":
                    data.at[gauge_id, "river_km"] = getval(row[1])
                if row[0] == "Ostwert:":
                    data.at[gauge_id, "X"] = getval(row[1])
                if row[0] == "Nordwert:":
                    data.at[gauge_id, "Y"] = getval(row[1])
                if row[0] == "Pegelnullpunktshöhe:":
                    data.at[gauge_id, "hp"] = getval(row[1])
                if row[0] == "Meldebeginn:":
                    data.at[gauge_id, "ms0"] = getval(row[1]) / 100
                if row[0] == "Meldestufe 1:":
                    data.at[gauge_id, "ms1"] = getval(row[1]) / 100
                if row[0] == "Meldestufe 2:":
                    data.at[gauge_id, "ms2"] = getval(row[1]) / 100
                if row[0] == "Meldestufe 3:":
                    data.at[gauge_id, "ms3"] = getval(row[1]) / 100
                if row[0] == "Meldestufe 4:":
                    data.at[gauge_id, "ms4"] = getval(row[1]) / 100

            try:
                gauge_hq = get_hnd_data(region, gauge + "-" + gauge_id)
            except:
                continue

            for idx, row in gauge_hq.iterrows():
                if row[0] in (
                    "HQ1",
                    "HQ2",
                    "HQ5",
                    "HQ10",
                    "HQ20",
                    "HQ50",
                    "HQ100",
                    "HQ1000",
                ):
                    data.at[gauge_id, row[0]] = getval(row[1])

            hnd = pd.concat((hnd, data))

    result = hnd.join(gkd, how="outer")
    # gauge_id is in index -> shift to column
    result["gauge_id"] = result.index

    return result


def main(district_id):
    """if called from commandline read, clip/buffer to district,
    and save data into common directory structure with prefix

    """

    # prepare directory structure
    print("preparing data for gis-mcda (c) tb.+la. 2024")
    print("preparing directory structure")
    prepare_directories(SRCPREFIX)
    prepare_directories(PREFIX)

    # warn if EU river database is not present
    if not os.path.exists(os.path.join(*SRC_FNS["rivers"])):
        print(
            f"""EU River database not found!

Please note that EU-Hydro requires a registration since late 2023
data is still open access but cannot be downloaded automatically

Please download at

    https://land.copernicus.eu/en/products/eu-hydro/eu-hydro-river-network-database

and save in

    {os.path.join(*SRC_FNS["rivers"])}/

Afterwards please restart the program.

"""
        )
        sys.exit(2)

    # Buffer size around study area in m
    BUF_SIZE = 10000

    # study area
    print("get study area")
    admin = get_admin()
    districts = admin.query(f"NUTS_ID.str.contains('{district_id}')")
    d = {
        "NUTS_ID": [
            district_id,
        ],
        "geometry": districts.unary_union.convex_hull,
    }
    studyarea = gpd.GeoDataFrame(d, crs=PROJECT_CRS)
    studyarea.to_file(fn.study_area)
    # drop FID and STATUS columns because they raise problems later on
    # studyarea.drop(columns=["FID"]).to_file(fn.study_area)
    study_buff = studyarea.buffer(BUF_SIZE)
    study_buff.drop(columns=["FID"]).to_file(fn.study_buff)

    # geology
    print("get geology for study area")
    geology_study = read_geology(studyarea)
    geology_study.to_file(fn.geology_study)

    # drillings
    print("get drilling data for study area")
    drill_coord = read_drill_coordinates(studyarea)
    drill_coord.to_file(fn.drillcoord_study)
    drill_layers = read_drill_layers(studyarea)
    drill_layers.to_csv(fn.drilllayers_study)
    drill_groundwater = read_drill_groundwater(studyarea)
    drill_groundwater.to_csv(fn.drillgw_study)
    drill_basedata = read_drill_basedata(studyarea)
    drill_basedata.to_csv(fn.drillbase_study)

    # landuse
    print("get landuse data for study area")
    landuse = read_landuse_actual(studyarea, admin)
    landuse.to_file(fn.landuse_study)

    # gauges
    print("get gauges in study area")
    gauges_all = read_gauge_shape(studyarea=None)
    gauges_all.to_file(fn.gauges_all)
    gauges_study = read_gauge_shape(studyarea)
    gauges_study.to_file(fn.gauges_study)
    gauges_csv = read_combined_gauges()
    gauges_csv.to_csv(fn.gauges_info, index=False)
    # gdf = gpd.GeoDataFrame(
    #     result,
    #     geometry=gpd.points_from_xy(result.X, result.Y),
    #     crs="EPSG:25832",
    # )
    # gdf.to_file(os.path.join(directory, "gauges","by-gkd-hnd.csv"))

    # rivers
    print("get rivers for study area")
    rivernet = read_rivernet(studyarea)
    rivernet.to_file(fn.rivers_study)
    rivernet = read_rivernet(study_buff)
    rivernet.to_file(fn.rivers_studybuffer)

    # protectedareas
    print("get protected areas in study area")
    print("   errors indicate that there are no protected zones")
    print("   of the given type in the study area.")
    file_list = []
    for protected in [
        "drinkingwater",
        "spawater",
        "ffh-areas",
        "bird-prot",
        "nationalparcs",
        "naturalpreserve",
        "landscapeprot",
        "natureparcs",
    ]:
        data = get_lfudata_generic(protected, studyarea)
        fname = f'{SRC_FNS[protected][-1].split(".")[0]}.gpkg'
        file_list.append(os.path.join("protection", fname))
        data.to_file(os.path.join("protection", fname))

    protected_combined = pd.concat(
        [gpd.read_file(shp) for shp in file_list],
        axis=0,
        ignore_index=True,
    ).pipe(gpd.GeoDataFrame)

    # drop duplicate column Status GPKG is not case sensitive
    protected_combined.drop(columns=["STATUS"]).to_file(fn.protected_all)

    # DEM
    print("get digital elevation model for study area")
    print(
        "this can take some time, please be patient or take a stroll at fresh air"
    )

    get_dem5(studyarea, buf=1001)

    # get elevation for river network
    print("get elevation for segments of river network")
    rivers = drape_rivernet()
    rivers.to_file(fn.rivers_dem)

    print("done")


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("""Usage: python prepare_gisdata.py NUTS_ID""")
    else:
        nuts_id = sys.argv[1]
        main(nuts_id)
