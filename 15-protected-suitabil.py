"""assign suitability with respect to protected areas

(c) 2023 la.

Data requirements:
+ map of study area
+ shape of all (!) protected areas in the study area

Results from previous analyses
+ constraint mask
"""

import pandas as pd
import geopandas as gpd
import gis_fn as fn

study_area = gpd.read_file(fn.study_area)
constraint_mask = gpd.read_file(fn.constraint_mask)
# this is the file with all protected areas
protectedareas = gpd.read_file(fn.protected_all)

notprotected = gpd.overlay(
    constraint_mask, protectedareas, how="difference", keep_geom_type=True
)
notprotected["suitabil"] = 1

protected = gpd.overlay(
    protectedareas, constraint_mask, how="intersection", keep_geom_type=True
)
# set suitability for protected areas to 0.5 (drinking water and
# national parks are already excluded as a constraint)
protected["suitabil"] = 0.5  


protectedareas_suitabil = pd.concat([protected, notprotected])
protectedareas_suitabil.to_file(fn.protectedareas_suitability)
