"""calculate flood dynamics at rivers based on data from gauges

(c) 2023 la.

interpolation/extrapolation is performed based on the cumulative
length of the river and its tributaries (which is a good proxy
for the catchment area)

Data requirements:
+ extent of study area (this will be handled automatically later)
+ geographical position of gauges
+ runoff data at gauges
+ rivers in study area (and in a buffer of x km around)

Results from previous analyses
+ constraint mask
"""

# tested and manually checked for swabia (DE27) and Unterallgäu
# (DE27C). The spatial extent of the study area has negiligible impact
# on the the overall suitability. The suitability for small areas
# (county level) is generally higher because of missing gauges and the
# default logic starting with suitable.
#
# We should not try to do a suitability analysis with this workflow on
# anything smaller than county level. Or we should increase the buffer
# for the river network to 20 / 50 km to make sure that we capture
# upstream and downstream gauges and rivers for community level
# analyses

import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point, LineString
from tqdm import tqdm
import gis_fn as fn
import mcda_config as cfg

# Read data
all_gauges = gpd.read_file(fn.gauges_all)
gauges = gpd.read_file(fn.gauges_study)
gauges_data = pd.read_csv(fn.gauges_info)
boundary_study = gpd.read_file(fn.study_area)
rivers = gpd.read_file(fn.rivers_study)
rivers_buffered = gpd.read_file(fn.rivers_studybuffer)
constraint_mask = gpd.read_file(fn.constraint_mask)

# Define parameters
# Below this discharge [m³/s] the suitability of a river is 1, above its 0
FLOOD_THRESHOLD = cfg.FLOOD_THRESHOLD
# Search in buffer of this distance [m] around gauging station for rivers
FLOOD_SEARCH_DIST = cfg.FLOOD_SEARCH_DIST

# River distance from 07-distance-constraint to avoid raster files with different sizes
BUFFER_DISTANCE = cfg.BUFFER_DISTANCE

# Process and expand gauges data
# exclude lake gauges and 12393201, as it is too close to the border
# (this is very specific to DE27 Swabia and might be dropped for the
# final code).
mask = (gauges["lage_am_ge"] != "Seepegel") & (
    gauges["stationsnu"] != 12393201
)
gauges = gauges[mask]  # Filter the GeoDataFrame using the mask
gauges_updated = gauges.merge(
    gauges_data, left_on="stationsnu", right_on="gauge_id", how="left"
)
gauges_updated["gauge_id"] = gauges_updated["gauge_id"].astype("Int64")


# Save relation between river id and next downstream and upstream river id
downstream_dict = dict(
    zip(rivers_buffered["OBJECT_ID"], rivers_buffered["NEXTDOWNID"])
)
upstream_dict = dict(
    zip(rivers_buffered["OBJECT_ID"], rivers_buffered["NEXTUPID"])
)  # Take rivers_buffered to include rivers at the edge of the study area


def calculate_floodvol(gauge):
    """calculate the difference in runoff between mean discharge MQ
    and 100-year discharge (HQ100) or general discharge at the highest
    specified water level if HQ100 is not given

    MQ is set zo zero if not given (worst case approach)
    """

    if not pd.isna(gauge["HQ100"]):
        hq_value = gauge["HQ100"]
    elif not pd.isna(gauge["HQ"]):
        hq_value = gauge["HQ"]
    else:
        hq_value = np.nan

    mq_value = gauge["MQ"] if not pd.isna(gauge["MQ"]) else 0

    return hq_value - mq_value


def assign_rivers_to_gauges():
    global rivers
    # Calculate volume of flood wave for each gauge
    gauges_updated["floodvol"] = gauges_updated.apply(
        calculate_floodvol, axis=1
    )

    # Create an empty list to store DataFrames for concatenation
    rivers_to_concat = []

    # Iterate through each gauge in the study area
    for idx, gauge in tqdm(
        gauges_updated.iterrows(), desc="assign rivers to gauges"
    ):
        point = gauge.geometry
        search_area = point.buffer(FLOOD_SEARCH_DIST)

        # Initialize variables to track nearest line and distance
        nearest_line = None
        min_distance = float("inf")

        # Check if any line intersects the search area
        for index, river in rivers_buffered.iterrows():
            line = river.geometry
            if line.intersects(search_area):
                # Calculate the distance between the point and the line
                distance = point.distance(line)

                # Update nearest line if the distance is smaller
                if distance < min_distance:
                    min_distance = distance
                    nearest_line = index

        if nearest_line is not None:
            # Cut the river segment at the nearest point
            river = rivers_buffered.loc[
                nearest_line
            ]  # find the nearest river in the dataframe of rivers in the study area
            nearest_point = river["geometry"].interpolate(
                river["geometry"].project(point)
            )
            upstream_segment, downstream_segment = split_river(
                river, nearest_point
            )

            if upstream_segment is None or downstream_segment is None:
                print(
                    f"River could not be splitted at gauge {idx} "
                    + f"Stationsnummer {gauge.loc['stationsnu']}"
                )
                gauges_updated.at[idx, "river_id"] = np.nan
            else:
                # Update the gauges_updated DataFrame with relevant information
                gauges_updated.at[idx, "river_id"] = river.loc["OBJECT_ID"]
                gauges_updated.at[idx, "disttoup"] = upstream_segment.length
                gauges_updated.at[idx, "disttodown"] = (
                    downstream_segment.length
                )
                gauges_updated.at[idx, "CUM_LEN"] = (
                    river.loc["CUM_LEN"] - downstream_segment.length
                )

                # Create new river IDs for the upstream and downstream segments
                upstream_river_id = f"{river.loc['OBJECT_ID']}-up"
                downstream_river_id = f"{river.loc['OBJECT_ID']}-down"

                # Create DataFrames for the upstream and downstream segments
                upstream_df = pd.DataFrame(
                    {
                        "OBJECT_ID": [upstream_river_id],
                        "geometry": [upstream_segment],
                        "LENGTH": [upstream_segment.length],
                        "CUM_LEN": [
                            river.loc["CUM_LEN"] - downstream_segment.length
                        ],
                        "STRAHLER": [river.loc["STRAHLER"]],
                    }
                )

                downstream_df = pd.DataFrame(
                    {
                        "OBJECT_ID": [downstream_river_id],
                        "geometry": [downstream_segment],
                        "LENGTH": [downstream_segment.length],
                        "CUM_LEN": [river.loc["CUM_LEN"]],
                        "STRAHLER": [river.loc["STRAHLER"]],
                    }
                )

                # Append DataFrames to the list
                rivers_to_concat.extend([upstream_df, downstream_df])
        else:
            print(
                f"No nearest river found for Pegel {idx} "
                + f"Stationsnummer {gauge.loc['stationsnu']}"
            )
            gauges_updated.at[idx, "river_id"] = np.nan

    # Concatenate DataFrames in the list
    if rivers_to_concat:
        new_rivers_df = pd.concat(rivers_to_concat, ignore_index=True)

        # Concatenate the new rivers DataFrame with the original rivers DataFrame
        rivers = pd.concat([rivers, new_rivers_df], ignore_index=True)

    # Save new geodataframe to a shapefile
    gauges_updated.to_file(fn.gauges_local)


def split_river(river, split_point):
    # Check if the geometry is a LineString
    if river["geometry"].geom_type == "LineString":
        coords = list(river["geometry"].coords)

        # Find the index of the nearest_point in the coordinates using
        # the min function and key parameter
        index_nearest = min(
            range(len(coords)),
            key=lambda i: split_point.distance(Point(coords[i])),
        )

        # Create upstream and downstream segments based on the index
        upstream_coords = coords[: index_nearest + 1]
        downstream_coords = coords[index_nearest:]

        # Create LineString objects for upstream and downstream segments
        upstream_segment = LineString(upstream_coords)
        # Check if downstream_coords has enough elements to form a valid LineString
        if len(downstream_coords) > 1:
            # Create LineString object for downstream segment
            downstream_segment = LineString(downstream_coords)
        else:
            # Handle the case where downstream_coords doesn't have enough elements
            downstream_segment = None

        return upstream_segment, downstream_segment

    return None, None


def update_downstream_suitability(river_id):
    downstream_id = downstream_dict.get(river_id)

    while downstream_id is not None:
        if downstream_id in rivers["OBJECT_ID"].values:
            rivers.loc[rivers["OBJECT_ID"] == downstream_id, "suitabil"] = 0
        if downstream_id in gauges_updated["river_id"].values:
            break
            # Then next downstream gauge is met and rest is handeled
            # by logic of this gauge
        downstream_id = downstream_dict.get(downstream_id)


def get_upstream_ids(gauge):
    upstream_columns = [f"up_id{j}" for j in range(20)]
    # Label of columns for upstream gauges
    present_columns = [
        column
        for column in upstream_columns
        if column in gauge.index and not pd.isna(gauge[column])
    ]

    if present_columns:
        up_gauges = gauge[present_columns].dropna().values.flatten()
        # Extract the last 8 numbers which is upstream ID
        up_gauges_ids = [up_id[-8:] for up_id in up_gauges]

        return up_gauges_ids

    return []


def calculate_reduced_discharge_and_update_suitability(
    river, cum_length, discharge, delta_discharge=None
):
    length = river["LENGTH"]
    threshold_point = None
    reduced_discharge = None

    for i in range(1, 11):
        # Check every 10%
        distance_up = length * (i / 10)
        if delta_discharge:
            # if known discharge of upstream gauges is given
            reduced_discharge = (
                discharge - delta_discharge * distance_up / cum_length
            )
        else:
            reduced_discharge = discharge * (1 - distance_up / cum_length)

        if reduced_discharge <= FLOOD_THRESHOLD:
            threshold_point = river.geometry.interpolate(distance_up)
            break  # Stop when the threshold is reached

    if threshold_point is None:
        rivers.loc[rivers["OBJECT_ID"] == river["OBJECT_ID"], "suitabil"] = 0
        check_river = True
    else:
        split_river_and_update_suitabil(river, threshold_point)
        check_river = False

    return reduced_discharge, check_river


def split_river_and_update_suitabil(river, threshold_point):
    global rivers
    # Split the river at the threshold point
    upstream_segment, downstream_segment = split_river(river, threshold_point)
    if river["OBJECT_ID"] == "RL35116000":
        print("h")

    if upstream_segment is not None:
        # Create DataFrames for the upstream segment
        upstream_df = pd.DataFrame(
            {
                "OBJECT_ID": [f"{river['OBJECT_ID']}-up"],
                "geometry": [upstream_segment],
                "suitabil": [1],
            }
        )
        # Concatenate DataFrames
        rivers = pd.concat([rivers, upstream_df], ignore_index=True)

    if downstream_segment is not None:
        # Create DataFrames for the downstream segment
        downstream_df = pd.DataFrame(
            {
                "OBJECT_ID": [f"{river['OBJECT_ID']}-down"],
                "geometry": [downstream_segment],
                "suitabil": [0],
            }
        )

        # Concatenate DataFrames
        rivers = pd.concat([rivers, downstream_df], ignore_index=True)


def calculate_extrapolated_discharge_and_update_suitability(
    river, cum_length, discharge
):
    length = river["LENGTH"]
    threshold_point = None

    for i in range(1, 11):
        # Check every 10%
        distance_down = length * (i / 10)
        extrapolated_discharge = discharge * (
            1 + distance_down / (cum_length - length + distance_down)
        )

        if extrapolated_discharge > FLOOD_THRESHOLD:
            threshold_point = river.geometry.interpolate(distance_down)
            break  # Stop when the threshold is reached

    if threshold_point is not None:
        split_river_and_update_suitabil(river, threshold_point)

    return extrapolated_discharge


def divert_discharge_to_tributaries(
    river_id, discharge, up_gauges_ids, up_gauge_at_main_river
):
    # Filter rivers DataFrame to find tributaries of the given river_id
    original_river_id = river_id[:10]
    tributaries_df = rivers[rivers["NEXTDOWNID"] == original_river_id].copy()
    total_tributary_length = tributaries_df["CUM_LEN"].sum()

    for idx, tributary in tributaries_df.iterrows():
        # Check if gauge at this tributary
        matching_gauge_id = find_gauge_at_main_river_upstream(
            tributary["OBJECT_ID"], up_gauges_ids
        )

        if (
            matching_gauge_id
            and matching_gauge_id[0] not in up_gauge_at_main_river
        ):
            matching_gauge_id = int(matching_gauge_id[0])
            matching_gauge = gauges_updated[
                gauges_updated["gauge_id"] == matching_gauge_id
            ].iloc[0]
            # Calculate discharge as extrapolation from cum_len and floodvol of gauge
            rate = matching_gauge["floodvol"] / matching_gauge["CUM_LEN"]
            diverted_discharge = rate * tributary["CUM_LEN"]

        else:
            # Calculate the proportion of the total discharge to divert to this tributary
            proportion = tributary["CUM_LEN"] / total_tributary_length
            diverted_discharge = discharge * proportion

        tributaries_df.loc[idx, "diverted_discharge"] = diverted_discharge

    return tributaries_df


def find_gauge_at_main_river_upstream(river_id, up_gauges_ids):
    matching_gauge_id = []

    # Convert the list of strings to integers
    up_gauges_ids_int = list(map(int, up_gauges_ids))
    up_gauges = gauges_updated[
        gauges_updated["gauge_id"].isin(up_gauges_ids_int)
    ]

    while river_id is not None and river_id in rivers["OBJECT_ID"].values:
        # Add the current upstream river's matching gauge IDs to the list
        matching_rows = up_gauges[up_gauges["river_id"] == river_id]
        matching_gauge_id.extend(map(str, matching_rows["gauge_id"].tolist()))

        if matching_gauge_id:
            break

        # Find all upstream rivers with the current river_id as 'NEXTDOWNID'
        upstream_rivers = rivers[rivers["NEXTDOWNID"] == river_id]

        if not upstream_rivers.empty:
            # Select the upstream river with the highest 'CUM_LEN'
            next_upstream_id = upstream_rivers.loc[
                upstream_rivers["CUM_LEN"].idxmax(), "OBJECT_ID"
            ]
            river_id = next_upstream_id
        else:
            # No more upstream rivers, exit the loop
            break

    return matching_gauge_id


def drop_splitted_rivers():
    global rivers

    for _, river in rivers.iterrows():
        upstream_river_id = f"{river['OBJECT_ID']}-up"

        # Check if upstream_river_id is in the 'OBJECT_ID' column of rivers
        if upstream_river_id in rivers["OBJECT_ID"].values:
            # Filter out the original river segment from rivers
            rivers = rivers[rivers["OBJECT_ID"] != river["OBJECT_ID"]]


def process_discharge(
    river,
    cum_len,
    discharge,
    up_gauges,
    up_gauge_at_main_river,
    delta_discharge=None,
):
    (
        reduced_discharge,
        check_river,
    ) = calculate_reduced_discharge_and_update_suitability(
        river, cum_len, discharge, delta_discharge
    )

    if check_river:
        tributaries_df = divert_discharge_to_tributaries(
            river["OBJECT_ID"],
            reduced_discharge,
            up_gauges,
            up_gauge_at_main_river,
        )

        if tributaries_df.empty:
            # No more tributaries -> last river segment before source
            (
                reduced_discharge,
                _,
            ) = calculate_reduced_discharge_and_update_suitability(
                river, cum_len, discharge, delta_discharge
            )
            # No more discharge processing necessary, as last segemnt
            # of tributary reached
            return

        tributaries_above_threshold = tributaries_df[
            tributaries_df["diverted_discharge"] > FLOOD_THRESHOLD
        ]

        if tributaries_above_threshold.empty:
            # If no tributary above threshold -> tributary suitability
            # = 1 -> no further calculation necessary
            return

        for _, tributary in tributaries_above_threshold.iterrows():
            river_id = tributary["OBJECT_ID"]
            cum_len = tributary["CUM_LEN"]
            discharge = tributary["diverted_discharge"]

            if not up_gauges:
                # No more upstream gauges -> recursively process
                # discharge for upstream rivers
                process_discharge(
                    tributary,
                    cum_len,
                    discharge,
                    up_gauges,
                    up_gauge_at_main_river,
                )

            elif up_gauges:
                # If upstream gauges were found for initial gauge
                gauge_ids_at_tributary = find_gauge_at_main_river_upstream(
                    river_id, up_gauges
                )

                if not gauge_ids_at_tributary:
                    process_discharge(
                        tributary,
                        cum_len,
                        discharge,
                        up_gauges,
                        up_gauge_at_main_river,
                    )

                elif gauge_ids_at_tributary:
                    # If upstream gauges were found for tributary
                    total_upstream_discharge, total_upstream_cum_len = 0, 0

                    for gauge_id in gauge_ids_at_tributary:
                        up_gauge = gauges_updated[
                            gauges_updated["gauge_id"] == int(gauge_id)
                        ].iloc[0]

                        if up_gauge["river_id"] == river_id:
                            # If upstream gauge is at the current river segment
                            delta_discharge = discharge - up_gauge["floodvol"]
                            reduced_cum_len = cum_len - up_gauge["CUM_LEN"]

                            downstream_river_id = f"{river_id}-down"
                            tributary_down_segment = rivers.loc[
                                rivers["OBJECT_ID"] == downstream_river_id
                            ].iloc[0]

                            (
                                reduced_discharge,
                                _,
                            ) = calculate_reduced_discharge_and_update_suitability(
                                tributary_down_segment,
                                reduced_cum_len,
                                discharge,
                                delta_discharge,
                            )
                            # This tributary does not need to assessed
                            # further, as rest is handeled by next
                            # gauge
                            return

                        else:
                            total_upstream_discharge += up_gauge["floodvol"]
                            total_upstream_cum_len += up_gauge["CUM_LEN"]

                    delta_discharge = discharge - total_upstream_discharge
                    reduced_cum_len = cum_len - total_upstream_cum_len

                    process_discharge(
                        tributary,
                        reduced_cum_len,
                        discharge,
                        up_gauges,
                        up_gauge_at_main_river,
                        delta_discharge,
                    )


def assign_river_suitability():
    # currently a suitable tributary (including its buffer) takes
    # precedence in an otherwise unsuitable river reach. Is this what
    # we want?

    global rivers
    rivers["suitabil"] = 1
    # make sure that the extrapolate variable is set
    extrapolate = False

    # Iterate through each gauge in study area
    for _, gauge in gauges_updated.iterrows():
        print("gauge " + str(gauge["gauge_id"]) + " checked")
        if pd.isna(gauge["river_id"]):
            ## TODO: check if we have to assign a suitability value if
            ## no river is assigned
            continue

        check_upstream_gauge = False

        river = rivers[rivers["OBJECT_ID"] == gauge["river_id"]].iloc[
            0
        ]  # Get river at which gauge is located
        river_id = river["OBJECT_ID"]

        # Get upstream segment for further discharge processing
        upstream_river_id = f"{river_id}-up"
        river_up_segment = rivers[
            rivers["OBJECT_ID"] == upstream_river_id
        ].iloc[0]
        # Set suitability for downstream segment also to 0
        downstream_river_id = f"{river_id}-down"
        river_down_segment = rivers[
            rivers["OBJECT_ID"] == downstream_river_id
        ].iloc[0]

        if gauge["floodvol"] > FLOOD_THRESHOLD:
            update_downstream_suitability(river_id)
            # Set downstream suitability to zero
            rivers.loc[
                rivers["OBJECT_ID"] == downstream_river_id, "suitabil"
            ] = 0
            up_gauges = get_upstream_ids(gauge)  # Get upstream gauges
            up_gauge_at_main_river = find_gauge_at_main_river_upstream(
                river_id, up_gauges
            )  # Get upstream gauges at main river

            if up_gauges:
                total_upstream_discharge, total_upstream_cum_len = 0, 0

                for up_id in up_gauges:
                    up_gauge = gauges_updated[
                        gauges_updated["gauge_id"] == int(up_id)
                    ]

                    if not up_gauge.empty:
                        up_gauge = up_gauge.iloc[0]

                        if up_id in up_gauge_at_main_river:
                            # Only take gauge at main river as fixed upstream point
                            total_upstream_discharge += up_gauge["floodvol"]
                            total_upstream_cum_len += up_gauge["CUM_LEN"]

                        if up_gauge["floodvol"] <= FLOOD_THRESHOLD:
                            # If flood volume > flood threshold, no further calculation necessary
                            check_upstream_gauge = True

                    else:
                        # If upstream gauge not in study area
                        process_discharge(
                            river_up_segment,
                            gauge["CUM_LEN"],
                            gauge["floodvol"],
                            up_gauges,
                            up_gauge_at_main_river,
                        )

            elif not up_gauges:
                process_discharge(
                    river_up_segment,
                    gauge["CUM_LEN"],
                    gauge["floodvol"],
                    up_gauges,
                    up_gauge_at_main_river,
                )

            if check_upstream_gauge:
                # Only True, if there are upstream gauges with
                # discharges lower than the flood threshold ->
                # suitability between gauges needs to be adapted
                delta_discharge = gauge["floodvol"] - total_upstream_discharge
                reduced_cum_len = gauge["CUM_LEN"] - total_upstream_cum_len

                process_discharge(
                    river_up_segment,
                    reduced_cum_len,
                    gauge["floodvol"],
                    up_gauges,
                    up_gauge_at_main_river,
                    delta_discharge,
                )

            elif not check_upstream_gauge:
                # If all upstream gauges > flood threshold, set
                # upstream segment also to suitability 0
                rivers.loc[
                    rivers["OBJECT_ID"] == upstream_river_id, "suitabil"
                ] = 0

        elif gauge["floodvol"] <= FLOOD_THRESHOLD:
            # No further calculation necessary, but check if down
            # gauges still in study area
            downstream_ids = []
            downstream_id = downstream_dict.get(river_id)

            while downstream_id is not None:
                if downstream_id in rivers["OBJECT_ID"].values:
                    downstream_ids.append(downstream_id)
                downstream_id = downstream_dict.get(downstream_id)

                if downstream_id in gauges_updated["river_id"].values:
                    # If gauge assigned to downstream river segment
                    extrapolate = False

            if extrapolate:
                # Only extrapolate discharge if next downstream gauge
                # is not met inside study area Extrapolated discharge
                # from gauge up to border of study area and check if
                # threshold is met
                extrapolated_discharge = (
                    calculate_extrapolated_discharge_and_update_suitability(
                        river_down_segment, gauge["CUM_LEN"], gauge["floodvol"]
                    )
                )

                for downstream_id in downstream_ids:
                    river = rivers[rivers["OBJECT_ID"] == downstream_id].iloc[
                        0
                    ]
                    extrapolated_discharge = calculate_extrapolated_discharge_and_update_suitability(
                        river, river["CUM_LEN"], extrapolated_discharge
                    )

    drop_splitted_rivers()
    rivers = rivers.set_crs(crs=25832)
    rivers.to_file(fn.river_suitability)


def assign_spatial_suitability():
    # Create empty GeoDataFrames
    buffered_1_gdf = gpd.GeoDataFrame(
        columns=["geometry", "suitabil"], crs=rivers.crs
    )
    buffered_0_gdf = gpd.GeoDataFrame(
        columns=["geometry", "suitabil"], crs=rivers.crs
    )

    # Iterate over each river
    for _, river in tqdm(rivers.iterrows(), desc="assign spatial suitability"):
        suitabil_value = river["suitabil"]

        # Buffer the river line geometry
        buffered_geometry = river.geometry.buffer(distance=BUFFER_DISTANCE)
        buffered_gdf = gpd.GeoDataFrame(
            geometry=[buffered_geometry], crs=rivers.crs
        )

        if suitabil_value == 1:
            if not buffered_1_gdf.empty:
                buffered_1_gdf = gpd.overlay(
                    buffered_1_gdf,
                    buffered_gdf,
                    how="union",
                    keep_geom_type=True,
                )
            else:
                buffered_1_gdf = pd.concat([buffered_1_gdf, buffered_gdf])

        elif suitabil_value == 0:
            if not buffered_0_gdf.empty:
                buffered_0_gdf = gpd.overlay(
                    buffered_0_gdf,
                    buffered_gdf,
                    how="union",
                    keep_geom_type=True,
                )
            else:
                buffered_0_gdf = pd.concat([buffered_0_gdf, buffered_gdf])

    buffered_1_gdf["suitabil"] = 1
    buffered_0_gdf["suitabil"] = 0

    buffered_0_gdf = gpd.overlay(
        buffered_0_gdf, buffered_1_gdf, how="difference", keep_geom_type=True
    )
    # If buffers overlap, buffer with suitability 1 stays
    buffered_gdf = pd.concat([buffered_1_gdf, buffered_0_gdf])

    # Overlay resulting suitability map with constraint mask and save
    # to gpkg file
    buffered_suitabil_gdf = buffered_gdf.overlay(
        constraint_mask, how="intersection", keep_geom_type=True
    )
    buffered_suitabil_gdf.to_file(fn.flooddyn_suitability)


def main():
    assign_rivers_to_gauges()
    print("rivers assigned to gauges")
    assign_river_suitability()
    print("suitability assigned to rivers")
    assign_spatial_suitability()
    print("suitability assigned spatially")


if __name__ == "__main__":
    main()
