import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon
from scipy.spatial import Voronoi


def generate_thiessen_polygons(points_gdf, polygon, studyarea):
    # Filter points within the current polygon
    points_within_polygon = points_gdf[points_gdf.within(polygon.geometry)]

    if not points_within_polygon.empty:
        # get geometry of these points
        points_coords = np.array(
            [point.coords[0] for point in points_within_polygon.geometry]
        )
        # add 4 points 10 km outside of the study area to account for
        # edges of voronoi diagrams
        env_coords = studyarea.envelope.get_coordinates()

        xl = int(min(env_coords["x"])) - 10000
        xr = int(max(env_coords["x"])) + 10000
        yb = int(min(env_coords["y"])) - 10000
        yt = int(max(env_coords["y"])) + 10000

        points_coords = np.append(
            points_coords,
            [
                [xl, yb],
                [xr, yb],
                [xr, yt],
                [xl, yt],
            ],
            axis=0,
        )

        # Create Delaunay triangulation using the points
        vor = Voronoi(points_coords)

        # Extract the Voronoi regions (Thiessen polygons)
        thiessen_polygons = []
        for region in vor.regions:
            if -1 not in region and len(region) > 0:
                polygon_coords = [vor.vertices[i] for i in region]
                pol = Polygon(polygon_coords)

                # Clip the Thiessen polygon to the boundary polygon
                clipped_polygon = pol.intersection(polygon.geometry)

                if not clipped_polygon.is_empty:
                    thiessen_polygons.append(clipped_polygon)

        # Create a GeoDataFrame for the Thiessen polygons with suitability value
        thiessen_gdf_list = []
        for i in range(len(thiessen_polygons)):
            drilling_in_polygon = points_within_polygon[
                points_within_polygon.within(thiessen_polygons[i])
            ]
            suitability_value = drilling_in_polygon["suitabil"].item()
            thiessen_gdf = gpd.GeoDataFrame(
                {
                    "geometry": [thiessen_polygons[i]],
                    "suitabil": [suitability_value],
                },
                crs=points_gdf.crs,
            )
            thiessen_gdf_list.append(thiessen_gdf)

        return gpd.GeoDataFrame(
            pd.concat(thiessen_gdf_list, ignore_index=True)
        )

    return gpd.GeoDataFrame()
