"""calculate height difference between rivers and aquifers and assign suitability

(c) 2023 la.

Data requirements:
+ drilling logs in study area
+ groundwater information in study area
+ river network in study area
+ dem for study area

Results from previous analyses
+ constraint mask
+ storage area constraint
"""

# TODO: Do we really need DGM1?
#       DGM5 is also available for download

import geopandas as gpd
import pandas as pd
import numpy as np
from shapely.geometry import Polygon, Point
from scipy.spatial import Voronoi
import gis_fn as fn
import mcda_config as cfg
from tqdm import tqdm

gpd.options.io_engine = "pyogrio"

DEM_BUFFER_DIST = cfg.DEM_BUFFER_DIST

rivers = gpd.read_file(fn.rivers_study)
studyarea = gpd.read_file(fn.study_area)
dgk_merged = gpd.read_file(fn.storage_capacity_constraint)
constraint_mask = gpd.read_file(fn.constraint_mask)
drillings = gpd.read_file(fn.drillcoord_study)
stammdaten = pd.read_csv(fn.drillbase_study)
groundwater = pd.read_csv(fn.drillgw_study)
drillings_sd = drillings.merge(stammdaten, on="ObjektID")
drillings_sd = drillings_sd.merge(groundwater, on="ObjektID")


# Print the resulting intersection GeoDataFrame
# intersection.to_file(rivers_dem_shp, driver='GPKG')

rivers_dem = gpd.read_file(fn.rivers_dem)


# Get height of groundwater table
for idx, drilling in drillings_sd.iterrows():
    # not sure if we still need the replacement, prepare_gisdata
    # should have handled this
    surface = float(str(drilling["Ansatzhoeh"]).replace(",", "."))
    endteufe = float(str(drilling["Endteufe_x"]))

    if surface == None:
        drillings_sd = drillings_sd.drop(idx)

    else:
        rwsp = float(str(drilling["RWSP"]).replace(",", "."))
        if drilling["GWerreicht"] == "Ja" and not np.isnan(rwsp):
            drillings_sd.loc[idx, "h_gwtable"] = surface - rwsp

        else:
            drillings_sd.loc[idx, "h_bottom"] = surface - endteufe

print("first part done")
# Deal with more drillings at the same location (keep drilling with
# highest suitability value): Create a dictionary to store unique
# drillings based on rounded geometry and suitability value
unique_drillings_dict = {}


# Define a function to round the coordinates of a Shapely Point
def round_geometry_coordinates(geometry):
    return Point(round(geometry.x, 0), round(geometry.y, 0))


for idx, drilling in drillings_sd.iterrows():
    point_geom = round_geometry_coordinates(drilling["geometry"])
    gwtable = drilling["h_gwtable"]
    bottom = drilling["h_bottom"]

    if point_geom in unique_drillings_dict:
        # Check if the current drilling has a higher groundwater table
        if gwtable > unique_drillings_dict[point_geom]["h_gwtable"]:
            unique_drillings_dict[point_geom] = {
                "geometry": point_geom,
                "h_gwtable": gwtable,
                "h_bottom": bottom,
            }
    else:
        unique_drillings_dict[point_geom] = {
            "geometry": point_geom,
            "h_gwtable": gwtable,
            "h_bottom": bottom,
        }

# Create a DataFrame from the unique drillings dictionary
drillings_sd = gpd.GeoDataFrame(
    list(unique_drillings_dict.values()), crs=drillings_sd.crs
)

drillings_sd.to_file(fn.groundwater_table)
print("second part done")

## Transfer point information to area
polygons_gdf = gpd.GeoDataFrame(
    columns=["geometry", "h_gwtable", "h_bottom"], crs=dgk_merged.crs
)

# Iterate through each geological unit
for index, polygon in dgk_merged.iterrows():
    # Filter points within the current polygon
    points_within_polygon = drillings_sd[drillings_sd.within(polygon.geometry)]

    if not points_within_polygon.empty:
        # get geometry of these points
        points_coords = np.array(
            [point.coords[0] for point in points_within_polygon.geometry]
        )
        # add 4 points 10 km outside of the study area to account for
        # edges of voronoi diagrams
        env_coords = studyarea.envelope.get_coordinates()

        xl = int(min(env_coords["x"])) - 10000
        xr = int(max(env_coords["x"])) + 10000
        yb = int(min(env_coords["y"])) - 10000
        yt = int(max(env_coords["y"])) + 10000

        points_coords = np.append(
            points_coords,
            [
                [xl, yb],
                [xr, yb],
                [xr, yt],
                [xl, yt],
            ],
            axis=0,
        )

        # Create Delaunay triangulation using the points
        vor = Voronoi(points_coords)

        # Extract the Voronoi regions (Thiessen polygons)
        thiessen_polygons = []
        for region in vor.regions:
            if -1 not in region and len(region) > 0:
                polygon_coords = [vor.vertices[i] for i in region]
                pol = Polygon(polygon_coords)

                # Clip the Thiessen polygon to the boundary polygon
                clipped_polygon = pol.intersection(polygon.geometry)

                if not clipped_polygon.is_empty:
                    thiessen_polygons.append(clipped_polygon)

        # Create a DataFrame for the Thiessen polygons with suitability value
        for i in range(len(thiessen_polygons)):
            drilling_in_polygon = points_within_polygon[
                points_within_polygon.within(thiessen_polygons[i])
            ]
            gwtable_value = drilling_in_polygon["h_gwtable"].item()
            bottom_value = drilling_in_polygon["h_bottom"].item()
            thiessen_gdf = gpd.GeoDataFrame(
                {
                    "geometry": [thiessen_polygons[i]],
                    "h_gwtable": [gwtable_value],
                    "h_bottom": [bottom_value],
                },
                crs=polygons_gdf.crs,
            )
            polygons_gdf = pd.concat(
                [polygons_gdf, thiessen_gdf], ignore_index=True
            )

        dgk_merged = dgk_merged.drop(index)

    else:
        dgk_merged.loc[index, "suitabil"] = 0.5

polygons_gdf = pd.concat([polygons_gdf, dgk_merged])
# Clip area to constraint mask
polygons_gdf = polygons_gdf.overlay(
    constraint_mask, how="intersection", keep_geom_type=True
)

print("third part done")

## Get height difference between river and groundwater table

## TODO: This is a candidate for parallization
##       Check if we really need 10 m resolution for the river segments
for idx, area in tqdm(polygons_gdf.iterrows(), total=polygons_gdf.shape[0]):
    polygon = area.geometry
    search_area = polygon.buffer(DEM_BUFFER_DIST)
    # print(f"Pegel {idx} with Stationsnummer {pegel_new_gdf.loc[idx,'stationsnu']}")

    # Initialize variables to track nearest line and distance
    nearest_line = None
    max_height = float("-inf")

    # create a spatial index for rivers_dem
    rivers_dem.sindex

    # use the spatial index for faster intersection checks
    for index in rivers_dem.sindex.intersection(search_area.bounds):
        line = rivers_dem.iloc[index].geometry
        if line.intersects(search_area):
            height = 0
            count = 0
            for coord in line.coords:
                height += coord[2]
                count += 1
            height /= count
            # height = rivers_dem.iloc[index]["ID"]

            # Update nearest line if height of river is bigger there
            if height > max_height:
                max_height = height
                nearest_line = index

    if nearest_line is not None:
        gwtable = area["h_gwtable"]
        drillbottom = area["h_bottom"]

        if not np.isnan(gwtable):
            height_diff = max_height - gwtable

            if height_diff < 0:  # river is lower than groundwater table
                polygons_gdf.at[idx, "suitabil"] = 0
            elif height_diff > 0:  # river is higher than groundwater table
                polygons_gdf.at[idx, "suitabil"] = 1

        elif np.isnan(gwtable):
            diff = max_height - drillbottom

            if diff < 0:  # river is lower than bottom of drilling
                polygons_gdf.at[idx, "suitabil"] = 0.5
            elif diff >= 0:  # river is higher than bottom of drilling
                polygons_gdf.at[idx, "suitabil"] = 1


print("fourth part done")

polygons_gdf.to_file(fn.elevation_suitability)
