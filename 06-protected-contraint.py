"""combine high value protection zones in study area 

(c) 2023 la.

Data requirements:
+ shape of study area
+ shape files for national parks
+ shape files for drinkingwater protection zones

Results from previous analyses
+ none
"""

import geopandas as gpd
import pandas as pd
import gis_fn as fn


# read extent of study_area
boundary_study = gpd.read_file(fn.study_area)

# nlp = gpd.read_file(nlp_shp) # this is handled by prepare_gisdata
nlp_study = gpd.read_file(fn.nationalparks)

# tsg = gpd.read_file(tsg_shp) # this is handled by prepare_gisdata
tsg_study = gpd.read_file(fn.drinkingwater)

protectedareas_constraint = gpd.GeoDataFrame(
    pd.concat([nlp_study, tsg_study], ignore_index=True), geometry="geometry"
)
protectedareas_constraint["suitabil"] = 0

# Perform the intersection between the boundary and merged polygons
# There is a warning we should check:
# 01_protected_constraint.py:45: UserWarning: `keep_geom_type=True` in
# overlay resulted in 65 dropped geometries of different geometry
# types than df1 has. Set `keep_geom_type=False` to retain all
# geometries

rest = gpd.overlay(
    boundary_study, protectedareas_constraint, how="symmetric_difference"
)
rest["suitabil"] = 1

protectedareas_gdf = pd.concat([protectedareas_constraint, rest])
# Column STATUS is duplicated and GPKG doesn't like duplicate columns at all
protectedareas_gdf.drop(columns=["Status"]).to_file(
    fn.protectedareas_constraint
)
