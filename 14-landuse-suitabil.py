"""assign suitability for landuse

(c) 2023 la.

Data requirements:
+ landuse data for study area

Results from previous analyses
+ constraint mask
"""

import geopandas as gpd
import gis_fn as fn
import mcda_config as cfg

constraint_mask = gpd.read_file(fn.constraint_mask)
landuse = gpd.read_file(fn.landuse_study)
landuse_suitabil = landuse.overlay(
    constraint_mask, how="intersection", keep_geom_type=True
)

PARTLY_SUITABLE_VEGETATION = cfg.PARTLY_SUITABLE_VEGETATION
UNSUITABLE_VEGETATION = cfg.UNSUITABLE_VEGETATION

landuse_suitabil["suitabil"] = landuse_suitabil["nutzart"].apply(
    lambda value: (
        0
        if value in UNSUITABLE_VEGETATION
        else (0.5 if value in PARTLY_SUITABLE_VEGETATION else 1)
    )
)
landuse_suitabil.to_file(fn.landuse_suitability)
