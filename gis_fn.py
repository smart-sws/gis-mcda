"""helper module to unify file names and keep all filenames in one
file

(c) 2024 tb./la.

TODO unify filenames and make other modules use them

usage:
import gis_fn as fn

constraint_mask = gpd.read_file(fn.constraint_mask)

we are assuming the following directory structure

current working directory w/ python scripts
  |
  |_ admin           : administrative boundaries
  |_ dem             : digital elevation models
     |_ tiles        : temporary folder for downloaded DEM tiles
  |_ drillings       : drilling logs and base data
  |_ protection      : protection zones
  |_ landuse         : landuse data
  |_ gauges          : information about gauges
  |_ rivernet        : copy of EU-Hydro for the catchment of the rivers
  |_ geology         : geological maps
  |_ results         : results of the simulation steps

"""
import os

# input files
study_area = os.path.join("admin", "study.gpkg")
study_buff = os.path.join("admin", "study_buff.gpkg")

geology_study = os.path.join("geology", "geology_study.gpkg")

drillcoord_study = os.path.join("drillings", "drillcoord_study.gpkg")
drilllayers_study = os.path.join("drillings", "drilllayers_study.csv")
drillgw_study = os.path.join("drillings", "drillgw_study.csv")
drillbase_study = os.path.join("drillings", "drillbase_study.csv")

landuse_study = os.path.join("landuse", "landuse_study.gpkg")
nationalparks = os.path.join("protection", "nlp_epsg25832_shp.gpkg")
drinkingwater = os.path.join("protection", "twsg_epsg25832.gpkg")
protected_all = os.path.join("protection", "protected-combined.gpkg")

rivers_study = os.path.join("rivernet", "rivers.gpkg")
rivers_studybuffer = os.path.join("rivernet", "rivers_studybuffer.gpkg")
rivers_buffered = os.path.join("rivernet", "rivers_buffered.gpkg")
rivers_dem = os.path.join("rivernet", "rivers_dem.gpkg")

gauges_all = os.path.join("gauges", "gauges_all.gpkg")
gauges_study = os.path.join("gauges", "gauges_study.gpkg")
gauges_info = os.path.join("gauges", "by-gkd-hnd.csv")
gauges_local = os.path.join("gauges", "gauges_local.gpkg")

dem1_study_tif = os.path.join("dem", "dem1_study.tif")
dem1_study = os.path.join("dem", "dem1_study.gpkg")

dem5_study_tif = os.path.join("dem", "dem5_study.tif")
dem5_study = os.path.join("dem", "dem5_study.gpkg")


# results
constraint_mask = os.path.join("results", "constraint-mask.gpkg")
storage_capacity_constraint = os.path.join("results", "storage-capacity.gpkg")
unsaturated_zone_constraint = os.path.join("results", "uzone-constraint.gpkg")
unsaturated_zone_thickness = os.path.join("results", "uzone-thickness.gpkg")
unsaturated_zone_suitability = os.path.join(
    "results", "uzone-suitability.gpkg"
)
aquifers = os.path.join("results", "aquifers.csv")
aquifer_thickness_suitability = os.path.join(
    "results", "aquifer-thickness-suitability.gpkg"
)
aquifer_thickness = os.path.join("results", "aquifer-thickness.gpkg")
hydraulic_conductivity = os.path.join("results", "hydraulic-conductivity.gpkg")
hydraulic_cond_values = os.path.join(
    "results", "hydraulic-conductivity-values.gpkg"
)
hydraulic_cond_constraint = os.path.join(
    "results", "hydraulic-cond-constraint.gpkg"
)
hydraulic_cond_suitability = os.path.join(
    "results", "hydraulic-cond-suitability.gpkg"
)
landuse_constraint = os.path.join("results", "landuse-constraint.gpkg")
landuse_suitability = os.path.join("results", "landuse-suitability.gpkg")
protectedareas = os.path.join("results", "protected-areas.gpkg")
protectedareas_constraint = os.path.join(
    "results", "protected-areas-constraint.gpkg"
)
protectedareas_suitability = os.path.join(
    "results", "protected-areas-suitability.gpkg"
)
river_distance = os.path.join("results", "river-distance-constraint.gpkg")
river_suitability = os.path.join(
    "results", "flooddynamics-river-suitabil.gpkg"
)
flooddyn_suitability = os.path.join("results", "flooddynamics-suitabil.gpkg")
elevation_suitability = os.path.join("results", "elevation-suitabil.gpkg")
groundwater_table = os.path.join("results", "groundwater-table.gpkg")


# final raster map
