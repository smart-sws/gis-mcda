"""combining suitability maps as raster maps

(c) 2023,2024 tb., la.

python suitability-map-tb2.py [RESOLUTION]

RESOLUTION is grid resolution in m, default 10

All shapefiles must have the same extent (otherwise the calculations
will fail). This could be handled here, but not now.

Although there are problems with displaying the tiffs on the command
line the tiffs load nicely in QGIS

Raster calculations checked against individual layers and manual
calculations in QGIS (manual result - automatic result = 0)

"""

import os
import sys
import shutil
import subprocess
import gc
from osgeo import gdal
import gis_fn as fn
import mcda_config as cfg

def shp_to_raster(infn, outfn, res):
    """read shapefile and convert to raster with given resolution"""

    options = gdal.RasterizeOptions(
        format="GTiff",
        xRes=res,
        yRes=res,
        noData=-9999,
        targetAlignedPixels=True,
        allTouched=True,
        attribute="suitabil",
        creationOptions=[
            "COMPRESS=ZSTD",
            "PREDICTOR=1",
            "ZLEVEL=9",
            "NUM_THREADS=16",
            "BIGTIFF=YES",
        ],
    )
    gdal.Rasterize(outfn, infn, options=options)


#    gdal_rasterize -l Erlaubnisse -a OBJECTID -tr 100.0 100.0 -a_nodata 0.0 -ot Float32 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=2 -co ZLEVEL=9 -at /home/tb/gis/bayern/bergbau/Erlaubnisse.shp /tmp/processing_ZVVJOe/f2cedded5bd8439baeb80b04e6940b28/OUTPUT.tif


def combine_raster(fn1, w1, fn2, w2, outfn):
    """add two raster layers with weights

    there should be a better way to do this without calling
    gdal_calc.py directly, sorry for that.
    """

    subprocess.run(
        [
            "gdal_calc.py",
            "--overwrite",
            "--calc",
            f"(A*{w1})+(B*{w2})",
            "--format",
            "GTiff",
            "--type",
            "Float32",
            "--NoDataValue",
            "-9999",
            "-A",
            fn1,
            "--A_band",
            "1",
            "-B",
            fn2,
            "--B_band",
            "1",
            "--outfile",
            outfn,
            "--co",
            "BIGTIFF=YES",
            "--co",
            "COMPRESS=ZSTD",
            "--co",
            "PREDICTOR=1",
            "--co",
            "ZLEVEL=9",
            "--co",
            "NUM_THREADS=16",
        ],
        shell=False,
        check=True,
    )

    # cmdline = gdal_calc.py --overwrite --calc "A+B" --format GTiff --type Float32 --NoDataValue 0.0 -A /home/tb/gis/smart-sws/elevation-suitabil.shp.tif --A_band 1 -B /home/tb/gis/smart-sws/elevation-suitabil.shp.tif --B_band 1 --outfile


if __name__ == "__main__":
    ## TODO: we could add options to save raster files for counties
    ##       or communities, or even to produce dynamic Tiffs with
    ##       increasing resolution as you zoom in.
    ##
    ##       Better: Adapt resolution to field-of-view. If we are going
    ##       to county level: increase resolution to 5m

    gdal.UseExceptions()
    KEEP_INTERMEDIATES = True
    if len(sys.argv) > 1:
        RESOLUTION = float(sys.argv[1])
    else:
        RESOLUTION = cfg.RESOLUTION

    OUTFN = os.path.join("results",f"result_{int(RESOLUTION)}m.tif")
    constraint_mask_shp = fn.constraint_mask
    # no longer needed
    # suitability_map_shp = os.path.join("suitabil_maps", "suitability-map")
    maps = {
        "uzone": fn.unsaturated_zone_suitability,
        "thickness": fn.aquifer_thickness_suitability,
        "kvalue": fn.hydraulic_cond_suitability,
        "landuse": fn.landuse_suitability,
        "protected": fn.protectedareas_suitability,
        "flooddyn": fn.flooddyn_suitability,
        "elevation": fn.elevation_suitability,
    }

    # weights = {
    #    "uzone": 0.104,
    #    "thickness": 0.068,
    #    "kvalue": 0.24,
    #    "landuse": 0.031,
    #    "protected": 0.045,
    #    "floodvol": 0.354,
    #    "elevation": 0.159,
    # }

    WEIGHTS = cfg.WEIGHTS

    i = 0
    for m in maps.keys():
        print(f"converting {m} to raster")
        shp_to_raster(maps[m], "tmp.tif", RESOLUTION)
        gc.collect()
        if i > 0:
            print(f"combining {m} w/ {OUTFN}")
            combine_raster("tmp.tif", WEIGHTS[m], OUTFN, 1.0, OUTFN)
        else:
            print(f"initialize {OUTFN} raster w/ {m}")
            combine_raster("tmp.tif", WEIGHTS[m], "tmp.tif", 0.0, OUTFN)

        if KEEP_INTERMEDIATES:
            shutil.copyfile("tmp.tif", os.path.join("results", f"{m}_{int(RESOLUTION)}m.tif"))

        i += 1
        gc.collect()
        os.remove("tmp.tif")

# suitabil_gdf.to_file(suitability_map_shp)

