# Europe
Administrative boundaries:  
https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts

River network:  
https://land.copernicus.eu/en/products/eu-hydro/eu-hydro-river-network-database

# Bavaria
Digital elevation model:  
https://geodaten.bayern.de/opengeodata/OpenDataDetail.html?pn=dgm1

Land use:  
https://geodaten.bayern.de/opengeodata/OpenDataDetail.html?pn=tatsaechlichenutzung

Drillings, Digital geological map, Protected areas, Gauging stations:  
https://www.umweltatlas.bayern.de/mapapps/resources/apps/umweltatlas/index.html?lang=de

Gauging stations:  
https://www.hnd.bayern.de/  
https://www.gkd.bayern.de/