"""find storage areas large enough to accept the given flood wave

(c) 2023 la.

Data requirements:
+ geological map of study area

Results from previous analyses
+ none
"""

import geopandas as gpd
from shapely import unary_union
import gis_fn as fn
import mcda_config as cfg


MIN_AREA = cfg.MIN_AREA  # m2 (3ha)

data = gpd.read_file(fn.geology_study)

data["area"] = data.geometry.area
data["suitabil"] = 0  # Initialize the "suitabil" column with default value 0

dropped_indices = set()  # Set to store the indices of dropped objects

for idx, row in data.iterrows():
    if idx not in dropped_indices:  # Skip dropped indices
        # Find neighboring objects using spatial intersection
        neighbors = data[data.geometry.intersects(row.geometry)].index.tolist()
        if idx in neighbors:
            neighbors.remove(idx)  # Remove self from neighbors list

        # Check if "stratkl2" values match between the original object and its neighbors
        original_stratkl2 = row["STRATKL2"]
        matching_neighbors = [
            neighbor
            for neighbor in neighbors
            if data.at[neighbor, "STRATKL2"] == original_stratkl2
        ]

        if matching_neighbors:
            # Merge original object with matching neighbors
            merged_geometry = unary_union(
                [row.geometry] + data.loc[matching_neighbors].geometry.tolist()
            )
            merged_object = gpd.GeoDataFrame(
                {
                    "geometry": [merged_geometry],
                    "area": [merged_geometry.area],
                    **row.drop(["geometry", "area"]),
                }
            )

            # Update the merged object in the GeoDataFrame
            merged_area = merged_object.geometry.unary_union.area
            data.at[idx, "area"] = merged_area
            data.at[idx, "geometry"] = merged_object["geometry"].iloc[0]

            # Remove the separated entries of the merged object
            data = data.drop(matching_neighbors)
            # Add the dropped indices to the set
            dropped_indices.update(matching_neighbors)

            if merged_area > MIN_AREA:
                data.at[
                    idx, "suitabil"
                ] = 1  # Write the value 1 to "suitabil" column

        if row["area"] > MIN_AREA:
            data.at[
                idx, "suitabil"
            ] = 1  # Write the value 1 to "suitabil" column

data.to_file(fn.storage_capacity_constraint)
