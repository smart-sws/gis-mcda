"""assign suitability for unsaturated thickness

(c) 2023 la.

Data requirements:
+ drilling logs in study area
+ groundwater information in study area

Results from previous analyses
+ constraint mask
+ storage area constraint
"""

import geopandas as gpd
import pandas as pd
from tqdm import tqdm
import gis_fn as fn


drillings = gpd.read_file(fn.drillcoord_study)
groundwater = pd.read_csv(fn.drillgw_study)
drillings_gw = drillings.merge(groundwater, on="ObjektID")

constraint_mask = gpd.read_file(fn.constraint_mask)
dgk_merged = gpd.read_file(fn.storage_capacity_constraint)

# Iterate through each geological unit
for index, polygon in tqdm(dgk_merged.iterrows(), total=dgk_merged.shape[0]):
    # Filter points within the current polygon
    points_within_polygon = drillings_gw[drillings_gw.within(polygon.geometry)]

    if not points_within_polygon.empty:
        dgk_merged.loc[index, "suitabil"] = 1

    else:  # if no points inside a geological unit
        dgk_merged.loc[index, "suitabil"] = 0.5

dgk_merged_suitabil = dgk_merged.overlay(
    constraint_mask, how="intersection", keep_geom_type=True
)
dgk_merged_suitabil.to_file(fn.unsaturated_zone_suitability)
