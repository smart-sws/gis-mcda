"""Configuration of MCDA for suitability mapping of stormwater infiltration sites

(c) 2023,2024 la. + tb.

This configuration file holds all criteria used in the MCDA. Please make
changes here and not in the specific subroutines.

Variables are listed in order of their first appearance
"""

# --------------------------------------------------
# 01-storagearea_constraint
# MIN_AREA is the minimum required aquifer area in m².
MIN_AREA = 30000

# --------------------------------------------------
# 02-uzone-constraint
# 12-thickness-suitabil
# minimum and maximum thickness of unsaturated zone in m
THRESHOLD_MIN = 2  # m
THRESHOLD_MAX = 20  # m

# --------------------------------------------------
# 03-aquifer-definition
# min thickness of the permeable layers in m to be counted as aquifer
AQUIFER_THRESHOLD = 2

# max thickness of impermeable lens in m before it is assumed
# to be hydraulically effective
LENS_THRESHOLD = 1

# define conductivity classes for each petrographic description
# this has to be adapted to your language and geological setting
CONDUCTIVITY_CLASSES = {
    "kies": 1,
    "lockergestein": 2,
    "sand": 3,
    "feinsand": 4,
    "schluff": 5,
    "ton": 6,
    "lehm": 6,
    "stein": 6,
    "konglomerat": 6,
    "mergel": 6,
    "organisch": 6,
}
# define maximum suitable HCC (1 is best, 7 is worst)
HCC_THRESHOLD = 3


# --------------------------------------------------
# 04-kvalue-constraint
# 13-kvalue-suitabil
# list for suitable stratigraphic units
# this list has to be adapted to your geological setting (and most likely language)
SUITABLE_UNITS = [
    "Pleistozän",
    "Pleistozän bis Holozän",
    "Pliozän bis Pleistozän",
    "Holozän",
]

# --------------------------------------------------
# 05-landuse-constraint
# 14-landuse-suitabil
# lists of acceptable and unsuitable landuses
# this has to be adapted to your locale settings
# categorize land uses
siedlung_0 = {
    "Wohnbaufläche",
    "Industrie- und Gewerbefläche",
    "Halde",
    "Tagebau, Grube, Steinbruch",
    "Fläche besonderer funktionaler Prägung",
    "Friedhof",
}
siedlung_1 = {
    "Fläche gemischter Nutzung",
    "Sport-, Freizeit- und Erholungsfläche",
}
verkehr_0 = {
    "Straßenverkehr",
    "Platz",
    "Bahnverkehr",
    "Flugverkehr",
    "Schiffsverkehr",
}
verkehr_1 = {"Weg"}
vegetation = {
    "Landwirtschaft",
    "Wald",
    "Gehölz",
    "Heide",
    "Moor",
    "Sumpf",
    "Unland/Vegetationslose Fläche",
}
vegetation_0 = {"Sumpf"}
vegetation_05 = {"Moor"}

gewaesser = {"Fließgewässer", "Hafenbecken", "Stehendes Gewässer"}

ACCEPTABLE_LANDUSE = vegetation | verkehr_1 | siedlung_1
PARTLY_SUITABLE_VEGETATION = vegetation_05
UNSUITABLE_VEGETATION = vegetation_0

# --------------------------------------------------
# 06-protected_constraint
# currently the logic relies on spatial data read from files
# we could probably construct an array to accept filenames with
# protected areas like:

import gis_fn as fn

PROTECTED_FILENAMES = [fn.drinkingwater, fn.nationalparks]

# --------------------------------------------------
# 07-river-distance-constraint
# maximum distance between storage site and river in m
# set to a large value to effectively remove this constraint
BUFFER_DISTANCE = 5000  # m

# --------------------------------------------------
# 16-flooddyn-suitabil
# maximum discharge at river in m³/s
FLOOD_THRESHOLD = 100
# search in buffer of this distance (in m) around gauging station for rivers
FLOOD_SEARCH_DIST = 100

# --------------------------------------------------
# 17-elevation-suitabil
# set buffer distance (adjust to fit your data)
DEM_BUFFER_DIST = 1000

# --------------------------------------------------
# 21-suitability-map
# default resolution of the final map in m
# note that this is a commandline option as well.
RESOLUTION = 10.0
# define weights for the suitabilities
WEIGHTS = {
    "uzone": 0.104,
    "thickness": 0.068,
    "kvalue": 0.24,
    "landuse": 0.031,
    "protected": 0.045,
    "flooddyn": 0.354,
    "elevation": 0.159,
}

# --------------------------------------------------
# 31-sensitivity-analysis
# range of variations for weights in fractions
import numpy as np

VARIATION_RANGE = np.arange(
    -0.20, 0.21, 0.10
)  # From -20% to +20% in 10% steps
